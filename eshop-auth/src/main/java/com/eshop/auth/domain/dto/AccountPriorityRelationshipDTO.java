package com.eshop.auth.domain.dto;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号和权限的关联关系DTO类
 * @date 2022-04-10
 */
@Data
public class AccountPriorityRelationshipDTO extends AbstractObject{

    /**
     * id
     */
    private Long id;
    /**
     * 账号id
     */
    private Long accountId;
    /**
     * 权限id
     */
    private Long priorityId;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
