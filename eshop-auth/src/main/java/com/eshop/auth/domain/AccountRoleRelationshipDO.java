package com.eshop.auth.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号角色关系DO类
 * @date 2022-04-10
 */
@Data
public class AccountRoleRelationshipDO {

    /**
     * id
     */
    private Long id;
    /**
     * 账号id
     */
    private Long accountId;
    /**
     * 角色id
     */
    private Long roleId;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
