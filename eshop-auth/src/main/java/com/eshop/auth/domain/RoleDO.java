package com.eshop.auth.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色DO类
 * @date 2022-04-10
 */
@Data
public class RoleDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 角色编号
     */
    private String code;
    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色备注
     */
    private String remark;
    /**
     * 角色的创建时间
     */
    private Date gmtCreate;
    /**
     * 角色的修改时间
     */
    private Date gmtModified;
}
