package com.eshop.auth.domain.dto;

import com.eshop.auth.enums.LoginWay;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 用户登录DTO类
 * @date 2022-04-10
 */
@Data
@ApiModel(value = "用户登录DTO类")
@NoArgsConstructor
public class UserLoginDTO implements Serializable {

    @NotBlank
    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("手机/邮箱验证码")
    private String validateCode;

    @ApiModelProperty("登录密码")
    private String password;

    @NotBlank
    @ApiModelProperty("uuid")
    private String uuid;

    @NotNull
    @ApiModelProperty(value = "登录类型PASSWORD（默认）:账号密码登录 MOBILE_CODE:手机验证码登录 EMAIL_CODE : 邮箱验证码登录")
    private LoginWay loginWay = LoginWay.PASSWORD;

}
