package com.eshop.auth.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author Elvis
 * @version 1.0
 * @description: 用户注册请求DTO
 * @date 2022-04-10
 */
@Data
@ApiModel(value = "用户注册请求DTO")
@NoArgsConstructor
public class UserRegisterDTO {

    @ApiModelProperty(value = "员工姓名", hidden = true)
    private String username;

    @NotBlank
    @ApiModelProperty(value = "用户名")
    private String name;

    @NotBlank
    @ApiModelProperty("验证码")
    private String validateCode;

    @NotBlank
    @ApiModelProperty("登录密码")
    private String password;

    @ApiModelProperty(value = "注册邮箱")
    private String email;

    @ApiModelProperty(value = "注册手机")
    private String mobile;
}
