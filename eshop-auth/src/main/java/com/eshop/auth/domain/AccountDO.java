package com.eshop.auth.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description:账号DO类
 * @date 2022-04-10
 */
@Data
public class AccountDO implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 员工姓名
     */
    private String name;
    /**
     * 账号备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;

}
