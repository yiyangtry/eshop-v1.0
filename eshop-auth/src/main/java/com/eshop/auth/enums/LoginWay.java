package com.eshop.auth.enums;

/**
 * @author Elvis
 * @version 1.0
 * @description: 登录方式
 * @date 2022-04-10
 */
public enum LoginWay {
    /**
     * 登录方式
     */
    PASSWORD,
    MOBILE_CODE,
    EMAIL_CODE
}
