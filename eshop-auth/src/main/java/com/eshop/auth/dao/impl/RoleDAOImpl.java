package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.RoleDAO;
import com.eshop.auth.domain.RoleDO;
import com.eshop.auth.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色管理模块DAO组件
 * @date 2022-04-10
 */
@Repository
public class RoleDAOImpl implements RoleDAO {

    /**
     * 角色管理模块mapper组件
     */
    @Autowired
    private RoleMapper roleMapper;

    /**
     * 新增角色
     * @param role 角色DO对象
     */
    @Override
    public Long save(RoleDO role) throws Exception {
        roleMapper.save(role);
        return role.getId();
    }
}
