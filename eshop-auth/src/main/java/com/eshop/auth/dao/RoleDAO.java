package com.eshop.auth.dao;

import com.eshop.auth.domain.RoleDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色管理模块DAO组件接口
 * @date 2022-04-10
 */
public interface RoleDAO {

    /**
     * 新增角色
     * @param role 角色DO对象
     * @return 角色id
     * @throws Exception
     */
    Long save(RoleDO role) throws Exception;
}
