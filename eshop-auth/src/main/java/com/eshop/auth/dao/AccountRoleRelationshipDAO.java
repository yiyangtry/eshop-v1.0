package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountRoleRelationshipDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号角色关系管理模块DAO组件接口
 * @date 2022-04-10
 */
public interface AccountRoleRelationshipDAO {

    /**
     * 新增账号和角色的关联关系
     * @param relation 账号和角色的关联关系
     */
    void save(AccountRoleRelationshipDO relation);
}
