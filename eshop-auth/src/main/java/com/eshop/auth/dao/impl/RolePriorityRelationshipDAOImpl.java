package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.RolePriorityRelationshipDAO;
import com.eshop.auth.domain.RolePriorityRelationshipDO;
import com.eshop.auth.mapper.RolePriorityRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色和权限关系管理模块的DAO组件
 * @date 2022-04-10
 */
@Repository
public class RolePriorityRelationshipDAOImpl implements RolePriorityRelationshipDAO {

    /**
     * 角色和权限关系管理模块的mapper组件
     */
    @Autowired
    private RolePriorityRelationshipMapper rolePriorityRelationshipMapper;

    /**
     * 新增账号和权限的关联关系
     * @param rolePriorityRelationshipDO
     */
    @Override
    public void save(RolePriorityRelationshipDO rolePriorityRelationshipDO) throws Exception {
        rolePriorityRelationshipMapper.save(rolePriorityRelationshipDO);
    }

}
