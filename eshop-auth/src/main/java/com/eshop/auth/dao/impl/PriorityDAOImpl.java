package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.PriorityDAO;
import com.eshop.auth.domain.PriorityDO;
import com.eshop.auth.mapper.PriorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 权限管理模块的DAO组件
 * @date 2022-04-10
 */
@Repository
public class PriorityDAOImpl implements PriorityDAO {


    /**
     * 权限管理模块的mapper组件
     */
    @Autowired
    private PriorityMapper priorityMapper;

    /**
     * 新增权限
     * @param priorityDO 权限DO对象
     */
    @Override
    public Long savePriority(PriorityDO priorityDO) throws Exception {
        priorityMapper.savePriority(priorityDO);
        return priorityDO.getId();
    }
}
