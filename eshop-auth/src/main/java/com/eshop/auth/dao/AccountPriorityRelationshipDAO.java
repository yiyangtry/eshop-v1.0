package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountPriorityRelationshipDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号和权限关系管理模块的DAO组件接口
 * @date 2022-04-10
 */
public interface AccountPriorityRelationshipDAO {

    /**
     * 新增账号和权限的关联关系
     * @param accountPriorityRelationshipDO
     */
    void save(AccountPriorityRelationshipDO accountPriorityRelationshipDO);
}
