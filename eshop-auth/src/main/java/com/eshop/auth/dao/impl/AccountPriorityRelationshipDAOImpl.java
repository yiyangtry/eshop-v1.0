package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.AccountPriorityRelationshipDAO;
import com.eshop.auth.domain.AccountPriorityRelationshipDO;
import com.eshop.auth.mapper.AccountPriorityRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号和权限关系管理模块的DAO组件
 * @date 2022-04-10
 */
@Repository
public class AccountPriorityRelationshipDAOImpl implements AccountPriorityRelationshipDAO {

    /**
     * 账号和权限关系管理模块的mapper组件
     */
    @Autowired
    private AccountPriorityRelationshipMapper accountPriorityRelationshipMapper;

    /**
     * 新增账号和权限的关联关系
     * @param accountPriorityRelationshipDO
     */
    @Override
    public void save(AccountPriorityRelationshipDO accountPriorityRelationshipDO) {
        accountPriorityRelationshipMapper.save(accountPriorityRelationshipDO);
    }
}
