package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.AccountDAO;
import com.eshop.auth.domain.AccountDO;
import com.eshop.auth.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理DAO组件
 * @date 2022-04-10
 */
@Repository
public class AccountDAOImpl implements AccountDAO {

    /**
     * 账号管理mapper组件
     */
    @Autowired
    private AccountMapper accountMapper;


    /**
     * 新增账号
     * @param account 账号
     */
    @Override
    public void save(AccountDO account) {
        accountMapper.save(account);
    }

    @Override
    public List<AccountDO> getUserInfoByUserName(String userName) {
        return accountMapper.getUserInfoByUserName(userName);
    }
}
