package com.eshop.auth.dao;

import com.eshop.auth.domain.RolePriorityRelationshipDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色和权限关系管理模块的DAO组件接口
 * @date 2022-04-10
 */
public interface RolePriorityRelationshipDAO {

    /**
     * 新增角色和权限的关联关系
     * @param rolePriorityRelationshipDO
     * @throws Exception
     */
    void save(RolePriorityRelationshipDO rolePriorityRelationshipDO) throws Exception;


}
