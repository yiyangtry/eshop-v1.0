package com.eshop.auth.dao.impl;

import com.eshop.auth.dao.AccountRoleRelationshipDAO;
import com.eshop.auth.domain.AccountRoleRelationshipDO;
import com.eshop.auth.mapper.AccountRoleRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号角色关系管理模块DAO组件
 * @date 2022-04-10
 */
@Repository
public class AccountRoleRelationshipDAOImpl implements AccountRoleRelationshipDAO {

    /**
     * 账号角色关系管理模块mapper组件
     */
    @Autowired
    private AccountRoleRelationshipMapper accountRoleRelationMapper;

    /**
     * 新增账号和角色的关联关系
     * @param relation 账号和角色的关联关系
     */
    @Override
    public void save(AccountRoleRelationshipDO relation) {
        accountRoleRelationMapper.save(relation);
    }
}
