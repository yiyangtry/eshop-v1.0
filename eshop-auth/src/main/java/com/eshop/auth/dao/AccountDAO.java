package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountDO;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理DAO组件接口
 * @date 2022-04-10
 */
public interface AccountDAO {

    /**
     * 新增账号
     * @param account 账号
     * @return 账号id
     */
    void save(AccountDO account);

    List<AccountDO> getUserInfoByUserName(String userName);
}
