package com.eshop.auth.dao;

import com.eshop.auth.domain.PriorityDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 权限管理模块的DAO组件接口
 * @date 2022-04-10
 */
public interface PriorityDAO {

    /**
     * 新增权限
     * @param priorityDO 权限DO对象
     * @return 权限id
     * @throws Exception
     */
    Long savePriority(PriorityDO priorityDO) throws Exception;
}
