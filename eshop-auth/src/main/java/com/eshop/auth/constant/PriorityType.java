package com.eshop.auth.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 权限类型
 * @date 2022-04-10
 */
public class PriorityType {

    /**
     * 菜单
     */
    public static final Integer MENU = 1;
    /**
     * 其他
     */
    public static final Integer OTHER = 2;

    private PriorityType() {

    }

}
