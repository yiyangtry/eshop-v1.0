package com.eshop.auth.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-10
 */
@Slf4j
@Configuration
@Data
public class SecurityConfig {

    @Value("${jwt.header}")
    private String header;

    @Value("${jwt.token-start-with}")
    private String tokenStartWith;

    @Value("${jwt.base64-secret}")
    private String base64Secret;

    @Value("${jwt.token-validity-in-seconds}")
    private int tokenValidityInSeconds;

    @Value("${jwt.online-key}")
    private String onlineKey;

    @Value("${jwt.code-key}")
    private String codeKey;

    @Bean
    public SecurityProperties securityProperties() {
        SecurityProperties securityProperties = new SecurityProperties();
        securityProperties.setHeader(header);
        securityProperties.setTokenStartWith(tokenStartWith);
        securityProperties.setBase64Secret(base64Secret);
        securityProperties.setTokenValidityInSeconds(tokenValidityInSeconds);
        securityProperties.setOnlineKey(onlineKey);
        securityProperties.setCodeKey(codeKey);
        return securityProperties;
    }
}
