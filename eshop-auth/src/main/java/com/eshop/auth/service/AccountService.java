package com.eshop.auth.service;

import com.eshop.auth.domain.AccountDO;
import com.eshop.auth.domain.dto.AccountDTO;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理service组件接口
 * @date 2022-04-10
 */
public interface AccountService {

    /**
     * 新增账号
     * @param account 账号
     * @return 处理结果
     * @throws Exception
     */
    void save(AccountDTO account) throws Exception;

    /**
     * 根据用户名查询用户信息
     * @param userName
     * @return
     */
    List<AccountDO> getUserInfoByUserName(String userName);
}
