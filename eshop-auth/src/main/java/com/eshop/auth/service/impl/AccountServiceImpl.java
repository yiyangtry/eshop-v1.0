package com.eshop.auth.service.impl;

import com.eshop.auth.dao.AccountDAO;
import com.eshop.auth.dao.AccountPriorityRelationshipDAO;
import com.eshop.auth.dao.AccountRoleRelationshipDAO;
import com.eshop.auth.domain.AccountDO;
import com.eshop.auth.domain.AccountPriorityRelationshipDO;
import com.eshop.auth.domain.AccountRoleRelationshipDO;
import com.eshop.auth.domain.dto.AccountDTO;
import com.eshop.auth.domain.dto.AccountPriorityRelationshipDTO;
import com.eshop.auth.domain.dto.AccountRoleRelationshipDTO;
import com.eshop.auth.service.AccountService;
import com.eshop.common.util.DateProvider;
import com.eshop.common.util.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理service组件
 * @date 2022-04-10
 */
@Service
public class AccountServiceImpl implements AccountService {

    /**
     * 账号管理DAO组件
     */
    @Autowired
    private AccountDAO accountDAO;

    /**
     * 账号和角色关系管理DAO组件
     */
    @Autowired
    private AccountRoleRelationshipDAO roleRelationDAO;

    /**
     * 账号和权限关系管理DAO组件
     */
    @Autowired
    private AccountPriorityRelationshipDAO priorityRelationDAO;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;


    /**
     * 新增账号
     *
     * @param account 账号
     * @return 处理结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(AccountDTO account) throws Exception {
        account.setGmtCreate(dateProvider.getCurrentTime());
        account.setGmtModified(dateProvider.getCurrentTime());
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        account.setId(idWorker.nextId());
        accountDAO.save(account.clone(AccountDO.class));
    }

    @Override
    public List<AccountDO> getUserInfoByUserName(String userName) {
        return accountDAO.getUserInfoByUserName(userName);
    }

    /**
     * 新增关联关系
     * @param account 账号
     * @throws Exception
     */
    private void saveRelations(AccountDTO account) throws Exception {
        for(AccountRoleRelationshipDTO roleRelation : account.getRoleRelations()) {
            roleRelation.setAccountId(account.getId());
            roleRelation.setGmtCreate(dateProvider.getCurrentTime());
            roleRelation.setGmtModified(dateProvider.getCurrentTime());
            roleRelationDAO.save(roleRelation.clone(AccountRoleRelationshipDO.class));
        }

        for(AccountPriorityRelationshipDTO priorityRelation : account.getPriorityRelations()) {
            priorityRelation.setAccountId(account.getId());
            priorityRelation.setGmtCreate(dateProvider.getCurrentTime());
            priorityRelation.setGmtModified(dateProvider.getCurrentTime());
            priorityRelationDAO.save(priorityRelation.clone(AccountPriorityRelationshipDO.class));
        }
    }
}