package com.eshop.auth.controller;

import com.eshop.auth.domain.dto.AccountDTO;
import com.eshop.auth.domain.vo.AccountVO;
import com.eshop.auth.service.AccountService;
import com.eshop.common.util.CloneDirection;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理controller组件
 * @date 2022-04-10
 */
@Slf4j
@Api(tags = "账号：账号管理")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 新增账号
     * @param account 账号
     * @return 处理结果
     */
    @PostMapping("/")
    public Boolean save(@RequestBody AccountVO account) {
        try {
            AccountDTO targetAccount = account.clone(AccountDTO.class,
                    CloneDirection.FORWARD);
            accountService.save(targetAccount);
            return true;
        } catch (Exception e) {
            log.error("error", e);
            return false;
        }
    }
}
