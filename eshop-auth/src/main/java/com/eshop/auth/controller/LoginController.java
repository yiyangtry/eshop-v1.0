package com.eshop.auth.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import com.eshop.auth.config.SecurityConfig;
import com.eshop.auth.domain.AccountDO;
import com.eshop.auth.domain.dto.AccountDTO;
import com.eshop.auth.domain.dto.UserLoginDTO;
import com.eshop.auth.domain.dto.UserRegisterDTO;
import com.eshop.auth.enums.LoginWay;
import com.eshop.auth.service.AccountService;
import com.eshop.common.util.*;
import com.eshop.auth.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Elvis
 * @version 1.0
 * @description: 注册登录退出模块
 * @date 2022-04-10
 */
@Slf4j
@Api(tags = "登录：注册登录退出模块")
@RestController
@RequestMapping(value = "user")
public class LoginController {

    @Autowired
    private AccountService accountService;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityConfig properties;

    @Autowired
    protected RedisUtils redisUtils;

    @ApiOperation("获取登录请求编码/防止恶意刷登录")
    @GetMapping(value = "/code")
    public JsonResult getCode() {
        return JsonResult.buildSuccess(getLoginCode());
    }

    /**
     * 获取登录请求编码
     *
     * @return
     */
    public Map<String, Object> getLoginCode() {
        // 获取运算的结果
        String uuid = properties.getCodeKey() + RandomUtil.randomString(8);
        String code = RandomUtil.randomString(6);
        // 保存
        redisUtils.set(uuid, code, 30, TimeUnit.MINUTES);
        // 验证码信息
        Map<String, Object> imgResult = new HashMap<>(2);
        imgResult.put("uuid", uuid);
        imgResult.put("code", code);
        return imgResult;
    }

    @ApiOperation("个人注册")
    @PostMapping(value = "/register")
    @Transactional(rollbackFor = Exception.class)
    public synchronized JsonResult register(@RequestBody @Valid UserRegisterDTO dto) throws Exception {
        if (Validator.isEmail(dto.getEmail())) {
            dto.setUsername(dto.getEmail());
            dto.setMobile(null);
        } else if (Validator.isMobile(dto.getMobile())) {
            dto.setUsername(dto.getMobile());
            dto.setEmail(null);
        } else {
            return JsonResult.buildError("USER_NOT_EXIST","账号格式不正确");
        }
        AccountDTO user = new AccountDTO();
        BeanUtil.copyProperties(dto,user);
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        user.setId(idWorker.nextId());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setGmtCreate(dateProvider.getCurrentTime());
        user.setGmtModified(dateProvider.getCurrentTime());
        accountService.save(user);
        return JsonResult.buildSuccess("个人注册成功");
    }

    @ApiOperation("登录授权")
    @PostMapping(value = "/login")
    public JsonResult login(@Validated @RequestBody UserLoginDTO userInfo, HttpServletRequest request) throws Exception {
        // 登录识别码
        String code = (String) redisUtils.get(userInfo.getUuid());
        LoginWay loginWay = userInfo.getLoginWay();


        List<AccountDO> users = accountService.getUserInfoByUserName(userInfo.getUsername());

        if (CollUtil.isEmpty(users)) {
            return JsonResult.buildError("USER_NOT_EXIST", "用户账号不存在");
        }

        if (users.size() > 1) {
            return JsonResult.buildError("USER_NOT_EXIST", "用户账号重复");
        }

        AccountDO accountDO = users.get(0);

        switch (loginWay) {
            case PASSWORD:
                // 密码解密
                String password = userInfo.getPassword();
                String userName = userInfo.getUsername();
                if (StringUtils.isBlank(code)) {
                    return JsonResult.buildError("REQUEST_CODE_ERROR", "请求uuid不存在或已过期");
                }
                if (!passwordEncoder.matches(userInfo.getPassword(), accountDO.getPassword())) {
                    return JsonResult.buildError("LOGIN_PASSWORD_ERROR", "登录密码错误");
                }
                break;
            default:
        }

        // 生成令牌
        String token = JwtTokenUtil.generateToken(new JWTInfo(accountDO.getUsername(), String.valueOf(accountDO.getId()), accountDO.getName()));

        // 返回 token 与 用户信息
        Map<String, Object> authInfo = new HashMap<>(2);

        authInfo.put("token", securityConfig.getTokenStartWith() + " " + token);
        authInfo.put("user", accountDO);
        return JsonResult.buildSuccess(authInfo);
    }
}
