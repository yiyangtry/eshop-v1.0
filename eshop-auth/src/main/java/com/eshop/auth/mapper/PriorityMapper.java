package com.eshop.auth.mapper;

import com.eshop.auth.domain.PriorityDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 权限管理模块的mapper组件
 * @date 2022-04-10
 */
@Mapper
public interface PriorityMapper {

    /**
     * 新增权限
     * @param priorityDO 权限DO对象
     */
    @Insert("INSERT INTO auth_priority("
            + "code, "
            + "url, "
            + "priority_comment, "
            + "priority_type, "
            + "parent_id, "
            + "gmt_create, "
            + "gmt_modified"
            + ") "
            + "VALUES("
            + "#{code}, "
            + "#{url}, "
            + "#{priorityComment}, "
            + "#{priorityType}, "
            + "#{parentId}, "
            + "#{gmtCreate}, "
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void savePriority(PriorityDO priorityDO);
}
