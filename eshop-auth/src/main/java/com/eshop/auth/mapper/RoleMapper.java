package com.eshop.auth.mapper;

import com.eshop.auth.domain.RoleDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: TODO
 * @date 2022-04-10
 */
@Mapper
public interface RoleMapper {

    /**
     * 新增角色
     * @param role 角色DO对象
     */
    @Insert("INSERT INTO auth_role("
            + "code,"
            + "name,"
            + "remark,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{code},"
            + "#{name},"
            + "#{remark},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(RoleDO role);
}
