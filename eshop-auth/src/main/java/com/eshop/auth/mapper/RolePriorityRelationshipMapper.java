package com.eshop.auth.mapper;

import com.eshop.auth.domain.RolePriorityRelationshipDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色和权限关系管理模块的mapper组件
 * @date 2022-04-10
 */
@Mapper
public interface RolePriorityRelationshipMapper {

    /**
     * 新增角色和权限的关联关系
     * @param rolePriorityRelationshipDO 角色和权限的关联关系
     */
    @Insert("INSERT INTO auth_role_priority_relationship("
            + "role_id,"
            + "priority_id,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{roleId},"
            + "#{priorityId},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(RolePriorityRelationshipDO rolePriorityRelationshipDO);
}
