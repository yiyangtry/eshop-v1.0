package com.eshop.auth.mapper;

import com.eshop.auth.domain.AccountDO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理mapper组件
 * @date 2022-04-10
 */
@Mapper
public interface AccountMapper {

    /**
     * 新增账号
     * @param account 账号
     */
    @Insert("INSERT INTO auth_account("
            + "id,"
            + "username,"
            + "password,"
            + "name,"
            + "remark,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{id},"
            + "#{username},"
            + "#{password},"
            + "#{name},"
            + "#{remark},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(AccountDO account);

    /**
     * 根据用户名查询用户信息
     * @param userName
     * @return
     */
    @Select("SELECT "
            + "id,"
            + "username,"
            + "password,"
            + "name,"
            + "remark,"
            + "gmt_create,"
            + "gmt_modified "
            + "FROM auth_account "
            + "WHERE username=#{userName}")
    @Results({
            @Result(column = "id", property = "id", id = true),
            @Result(column = "username", property = "username"),
            @Result(column = "password", property = "password"),
            @Result(column = "name", property = "name"),
            @Result(column = "remark", property = "remark"),
            @Result(column = "gmt_create", property = "gmtCreate"),
            @Result(column = "gmt_modified", property = "gmtModified")
    })
    List<AccountDO> getUserInfoByUserName(@Param("userName") String userName);
}
