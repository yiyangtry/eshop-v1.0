package com.eshop.auth.mapper;

import com.eshop.auth.domain.AccountRoleRelationshipDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号角色关系管理模块mapper组件
 * @date 2022-04-10
 */
@Mapper
public interface AccountRoleRelationshipMapper {

    /**
     * 新增账号和角色的关联关系
     * @param relation 账号和角色的关联关系
     */
    @Insert("INSERT INTO auth_account_role_relationship("
            + "account_id,"
            + "role_id,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{accountId},"
            + "#{roleId},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(AccountRoleRelationshipDO relation);
}
