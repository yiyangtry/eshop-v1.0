package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountRoleRelationshipDO;
import com.eshop.common.util.DateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号和角色关系管理模块的DAO组件的单元测试类
 * @date 2022-04-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRoleRelationshipDaoTest {

    /**
     * 账号和角色关系管理模块的DAO组件
     */
    @Autowired
    private AccountRoleRelationshipDAO accountRoleRelationshipDAO;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 测试新增账号和角色的关联关系
     * @throws Exception
     */
    @Test
    public void testSave() throws Exception {
        Long accountId = 1L;
        Long roleId = 1L;
        AccountRoleRelationshipDO accountRoleRelationshipDO =
                createAccountRoleRelationshipDO(accountId, roleId);
    }

    /**
     * 创建账号和角色关系DO对象
     * @return 账号和角色关系DO对象
     * @throws Exception
     */
    private AccountRoleRelationshipDO createAccountRoleRelationshipDO(
            Long accountId, Long roleId) throws Exception {
        AccountRoleRelationshipDO accountRoleRelationshipDO =
                new AccountRoleRelationshipDO();
        accountRoleRelationshipDO.setAccountId(accountId);
        accountRoleRelationshipDO.setRoleId(roleId);
        accountRoleRelationshipDO.setGmtCreate(dateProvider.getCurrentTime());
        accountRoleRelationshipDO.setGmtModified(dateProvider.getCurrentTime());

        accountRoleRelationshipDAO.save(accountRoleRelationshipDO);

        return accountRoleRelationshipDO;
    }
}
