package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountPriorityRelationshipDO;
import com.eshop.common.util.DateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Elvis
 * @version 1.0
 * @description: 账号和权限关系管理模块的DAO组件的单元测试类
 * @date 2022-04-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountPriorityRelationshipDaoTest {

    /**
     * 账号和权限关系管理模块的DAO组件
     */
    @Autowired
    private AccountPriorityRelationshipDAO accountPriorityRelationshipDAO;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 测试新增账号和权限的关联关系
     * @throws Exception
     */
    @Test
    public void testSave() throws Exception {
        Long accountId = 1L;
        Long priorityId = 1L;
        AccountPriorityRelationshipDO accountPriorityRelationshipDO =
                createAccountPriorityRelationshipDO(accountId, priorityId);
    }

    /**
     * 创建账号和权限关系DO对象
     * @return 账号和权限关系DO对象
     * @throws Exception
     */
    private AccountPriorityRelationshipDO createAccountPriorityRelationshipDO(
            Long accountId, Long priorityId) throws Exception {
        AccountPriorityRelationshipDO accountPriorityRelationshipDO =
                new AccountPriorityRelationshipDO();
        accountPriorityRelationshipDO.setAccountId(accountId);
        accountPriorityRelationshipDO.setPriorityId(priorityId);
        accountPriorityRelationshipDO.setGmtCreate(dateProvider.getCurrentTime());
        accountPriorityRelationshipDO.setGmtModified(dateProvider.getCurrentTime());

        accountPriorityRelationshipDAO.save(accountPriorityRelationshipDO);

        return accountPriorityRelationshipDO;
    }
}
