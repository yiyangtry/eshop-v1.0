package com.eshop.auth.dao;

import com.eshop.auth.domain.RoleDO;
import com.eshop.common.util.DateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Elvis
 * @version 1.0
 * @description: 角色管理模块DAO组件的单元测试类
 * @date 2022-04-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleDaoTest {

    /**
     * 角色管理模块DAO组件
     */
    @Autowired
    private RoleDAO roleDAO;
    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 测试新增角色
     * @throws Exception
     */
    @Test
    public void testSave() throws Exception {
        RoleDO role = createRole("测试角色", "TEST_CODE");
    }

    /**
     * 创建角色
     * @throws Exception
     */
    private RoleDO createRole(String name, String code) throws Exception {
        RoleDO role = new RoleDO();
        name = "超级管理员";
        code = "admin";
        role.setName(name);
        role.setCode(code);
        role.setRemark(name);
        role.setGmtCreate(dateProvider.getCurrentTime());
        role.setGmtModified(dateProvider.getCurrentTime());

        roleDAO.save(role);

        return role;
    }
}
