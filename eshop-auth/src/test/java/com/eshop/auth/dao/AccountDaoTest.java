package com.eshop.auth.dao;

import com.eshop.auth.domain.AccountDO;
import com.eshop.common.util.DateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;




/**
 * @author Elvis
 * @version 1.0
 * @description: 账号管理DAO组件的单元测试
 * @date 2022-04-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountDaoTest {

    /**
     * 账号管理DAO组件
     */
    @Autowired
    private AccountDAO accountDAO;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 测试新增账号
     * @throws Exception
     */
    @Test
    public void testSave() throws Exception {
        AccountDO account = createAccount();
    }

    /**
     * 创建账号
     * @return 账号
     * @throws Exception
     */
    private AccountDO createAccount() throws Exception {
        AccountDO account = new AccountDO();
        account.setUsername("zhangsan");
        account.setPassword("12345678");
        account.setName("张三");
        account.setRemark("测试账号");
        account.setGmtCreate(dateProvider.getCurrentTime());
        account.setGmtModified(dateProvider.getCurrentTime());

        accountDAO.save(account);

        return account;
    }
}
