package com.eshop.auth.dao;

import com.eshop.auth.constant.PriorityType;
import com.eshop.auth.domain.AccountPriorityRelationshipDO;
import com.eshop.auth.domain.PriorityDO;
import com.eshop.auth.domain.RolePriorityRelationshipDO;
import com.eshop.common.util.DateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Elvis
 * @version 1.0
 * @description: 权限管理模块的DAO组件的单元测试类
 * @date 2022-04-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PriorityDaoTest {

    /**
     * 权限管理模块的DAO组件
     */
    @Autowired
    private PriorityDAO priorityDAO;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 角色管理DAO组件
     */
    @Autowired
    private RoleDAO roleDAO;

    /**
     * 角色和权限关系管理DAO组件
     */
    @Autowired
    private RolePriorityRelationshipDAO rolePriorityRelationDAO;

    /**
     * 账号管理DAO组件
     */
    @Autowired
    private AccountDAO accountDAO;

    /**
     * 账号和角色关系管理组件
     */
    @Autowired
    private AccountRoleRelationshipDAO accountRoleRelationDAO;

    /**
     * 账号和权限关系管理组件
     */
    @Autowired
    private AccountPriorityRelationshipDAO accountPriorityRelationDAO;

    /**
     * 测试新增权限
     * @throws Exception
     */
    @Test
    public void testSavePriority() throws Exception {
        Long prarentId = 1L;
        PriorityDO priorityDO = createPriorityDO(prarentId);
    }

    /**
     * 构造一个权限DO对象
     * @return 权限DO对象
     * @throws Exception
     */
    private PriorityDO createPriorityDO(Long parentId) throws Exception {
        Random random = new Random();
        int randomInt = random.nextInt() * 100;

        PriorityDO priorityDO = new PriorityDO();
        priorityDO.setCode("TEST_" + randomInt);
        priorityDO.setGmtCreate(dateProvider.getCurrentTime());
        priorityDO.setGmtModified(dateProvider.getCurrentTime());
        priorityDO.setParentId(parentId);
        priorityDO.setPriorityComment("TEST_" + randomInt);
        priorityDO.setPriorityType(PriorityType.MENU);
        priorityDO.setUrl("http://127.0.0.1/" + randomInt);

        priorityDAO.savePriority(priorityDO);

        return priorityDO;
    }

    /**
     * 创建账号和权限关系DO对象
     * @return 账号和权限关系DO对象
     * @throws Exception
     */
    @Test
    public void createAccountPriorityRelation() throws Exception {
        AccountPriorityRelationshipDO accountPriorityRelationshipDO =
                new AccountPriorityRelationshipDO();
        accountPriorityRelationshipDO.setAccountId(54L);
        accountPriorityRelationshipDO.setPriorityId(25L);
        accountPriorityRelationshipDO.setGmtCreate(dateProvider.getCurrentTime());
        accountPriorityRelationshipDO.setGmtModified(dateProvider.getCurrentTime());

        accountPriorityRelationDAO.save(accountPriorityRelationshipDO);

    }

    /**
     * 创建角色和权限关系DO对象
     * @return 角色和权限关系DO对象
     * @throws Exception
     */
    @Test
    public void createRolePriorityRelation() throws Exception {
        RolePriorityRelationshipDO rolePriorityRelationshipDO =
                new RolePriorityRelationshipDO();
        rolePriorityRelationshipDO.setRoleId(9L);
        rolePriorityRelationshipDO.setPriorityId(25L);
        rolePriorityRelationshipDO.setGmtCreate(dateProvider.getCurrentTime());
        rolePriorityRelationshipDO.setGmtModified(dateProvider.getCurrentTime());

        rolePriorityRelationDAO.save(rolePriorityRelationshipDO);

    }
}
