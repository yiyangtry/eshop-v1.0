package com.eshop.order.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 是否发表过评论
 * @date 2022-04-16
 */
public class PublishedComment {

	/**
	 * 发表过评论
	 */
	public static final Integer YES = 1;
	/**
	 * 没有发表过评论
	 */
	public static final Integer NO = 0;
	
	private PublishedComment() {
		
	}
	
}
