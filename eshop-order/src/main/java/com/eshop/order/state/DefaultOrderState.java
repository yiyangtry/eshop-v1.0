package com.eshop.order.state;


import com.eshop.common.util.DateProvider;
import com.eshop.order.constant.OrderStatus;
import com.eshop.order.dao.OrderInfoDAO;
import com.eshop.order.domain.dto.OrderInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Elvis
 * @version 1.0
 * @description: 默认的订单状态组件
 * @date 2022-04-16
 */
@Component
public class DefaultOrderState extends AbstractOrderState {

	@Autowired
	public DefaultOrderState(DateProvider dateProvider, OrderInfoDAO orderInfoDAO) {
		super(dateProvider, orderInfoDAO);
	}
	
	@Override
	protected Integer getOrderStatus(OrderInfoDTO order) throws Exception {
		return OrderStatus.UNKNOWN;
	}
	
}
