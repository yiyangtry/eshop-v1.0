package com.eshop.order.domain.dto;

import lombok.Data;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单查询条件
 * @date 2022-04-16
 */
@Data
public class OrderInfoQuery {

    /**
     * 分页查询起始位置
     */
    private Integer offset;
    /**
     * 每页显示数据条数
     */
    private Integer size;
    /**
     * 用户账号id
     */
    private Long userAccountId;
}
