package com.eshop.order;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单服务
 * @date 2022-04-16
 */
@EnableSwagger2Doc
@EnableSwaggerBootstrapUI
@SpringBootApplication
@EnableDiscoveryClient
public class OrderBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(OrderBootstrap.class, args);
    }
}
