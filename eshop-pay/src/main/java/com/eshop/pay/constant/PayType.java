package com.eshop.pay.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付方式
 * @date 2022-04-16
 */
public class PayType {

    /**
     * 支付宝
     */
    public static final Integer ALIPAY = 1;
    /**
     * 微信支付
     */
    public static final Integer WEIXIN_PAY = 2;

    private PayType() {

    }

}
