package com.eshop.pay;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付服务
 * @date 2022-04-15
 */
@EnableSwagger2Doc
@EnableSwaggerBootstrapUI
@SpringBootApplication
@EnableDiscoveryClient
public class PayBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(PayBootstrap.class, args);
    }
}
