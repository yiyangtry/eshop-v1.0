package com.eshop.pay.domain.dto;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付交易流水
 * @date 2022-04-15
 */
@Data
public class PayTransactionDTO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 订单id
     */
    private Long orderInfoId;
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 订单应付金额
     */
    private Double payableAmount;
    /**
     * 用户账号id
     */
    private Long userAccountId;
    /**
     * 用户支付账号
     */
    private String userPayAccount;
    /**
     * 交易渠道
     */
    private Integer transactionChannel;
    /**
     * 第三方支付交易编号
     */
    private String transactionNumber;
    /**
     * 第三方支付完成支付的时间
     */
    private Date finishPayTime;
    /**
     * 第三方支付的响应状态码
     */
    private String responseCode;
    /**
     * 支付交易状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
