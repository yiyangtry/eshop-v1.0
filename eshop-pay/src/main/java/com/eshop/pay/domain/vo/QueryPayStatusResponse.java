package com.eshop.pay.domain.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 查询支付状态的响应结果
 * @date 2022-04-15
 */
@Data
public class QueryPayStatusResponse {

    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 用户支付账号
     */
    private String userPayAccount;
    /**
     * 第三方支付交易流水号
     */
    private String transactionNumber;
    /**
     * 第三方支付完成支付的时间
     */
    private Date finishPayTime;
    /**
     * 第三方支付响应状态码
     */
    private String responseCode;
    /**
     * 支付交易状态
     */
    private Integer payTransactionStatus;

}