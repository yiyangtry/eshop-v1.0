package com.eshop.pay.domain.dto;

import lombok.Data;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付交易流水查询条件
 * @date 2022-04-15
 */
@Data
public class PayTransactionQuery {

    /**
     * 分页查询起始位置
     */
    private Integer offset;
    /**
     * 每页显示的数据条数
     */
    private Integer size;
}
