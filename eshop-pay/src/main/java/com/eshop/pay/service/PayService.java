package com.eshop.pay.service;

import com.eshop.order.domain.dto.OrderInfoDTO;
import com.eshop.wms.domain.dto.ReturnGoodsInputOrderDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付中心接口
 * @date 2022-04-15
 */
public interface PayService {

    /**
     * 获取支付二维码
     * @param order 订单
     * @return 支付二维码
     */
    String getQrCode(OrderInfoDTO order);

    /**
     * 进行退款
     * @param returnGoodsInputOrder 退货入库单
     * @return 退款结果
     */
    Boolean refund(ReturnGoodsInputOrderDTO returnGoodsInputOrder);

}
