package com.eshop.pay.controller;

import com.eshop.common.util.ObjectUtils;
import com.eshop.pay.domain.dto.PayTransactionQuery;
import com.eshop.pay.service.PayTransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 支付交易流水管理controller组件
 * @date 2022-04-15
 */
@Slf4j
@RestController
@RequestMapping("/pay/transaction")
public class PayTransactionController {

    /**
     * 支付交易流水管理service组件
     */
    @Autowired
    private PayTransactionService payTransactionService;

    /**
     * 分页查询支付交易流水
     * @param query 查询条件
     * @return 支付交易流水
     */
    @GetMapping("/")
    public List<PayTransactionVO> listByPage(PayTransactionQuery query) {
        try {
            return ObjectUtils.convertList(
                    payTransactionService.listByPage(query),
                    PayTransactionVO.class);
        } catch (Exception e) {
            log.error("error", e);
            return null;
        }
    }
}
