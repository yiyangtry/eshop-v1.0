package com.eshop.schedule.service;

import com.eshop.order.domain.dto.OrderItemDTO;
import com.eshop.schedule.domain.dto.SaleDeliveryScheduleResult;

/**
 * @author Elvis
 * @version 1.0
 * @description: 销售出库调度器接口
 * @date 2022-04-16
 */
public interface SaleDeliveryScheduler {
	
	/**
	 * 调度销售出库
	 * @param orderItem 订单条目
	 * @return 调度结果
	 * @throws Exception
	 */
	SaleDeliveryScheduleResult schedule(OrderItemDTO orderItem) throws Exception;
	
	/**
	 * 获取订单条目的调度结果
	 * @param orderItem 订单条目
	 * @return 调度结果
	 * @throws Exception
	 */
	SaleDeliveryScheduleResult getScheduleResult(OrderItemDTO orderItem) throws Exception;

}
