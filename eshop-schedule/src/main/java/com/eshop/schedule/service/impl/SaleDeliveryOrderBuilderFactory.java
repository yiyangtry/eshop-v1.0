package com.eshop.schedule.service.impl;

import com.eshop.common.bean.SpringApplicationContext;
import com.eshop.schedule.service.SaleDeliveryOrderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author Elvis
 * @version 1.0
 * @description: 销售出库单builder工厂
 * @date 2022-04-16
 */
@Component
public class SaleDeliveryOrderBuilderFactory {

	/**
	 * spring容器
	 */
	@Autowired
	private SpringApplicationContext context;
	
	/**
	 * 获取一个销售出库单builder
	 * @return 销售出库单builder
	 */
	public SaleDeliveryOrderBuilder get() {
		return context.getBean(DefaultSaleDeliveryOrderBuilder.class);
	}
	
}
