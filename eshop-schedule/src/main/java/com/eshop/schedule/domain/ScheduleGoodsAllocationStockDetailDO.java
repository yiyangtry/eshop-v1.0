package com.eshop.schedule.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 调度中心的货位库存明细
 * @date 2022-04-17
 */
@Data
public class ScheduleGoodsAllocationStockDetailDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 货位id
     */
    private Long goodsAllocationId;
    /**
     * 上架时间
     */
    private Date putOnTime;
    /**
     * 上架数量
     */
    private Long putOnQuantity;
    /**
     * 当前剩余库存数量
     */
    private Long currentStockQuantity;
    /**
     * 当前锁定的库存数量
     */
    private Long lockedStockQuantity;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
