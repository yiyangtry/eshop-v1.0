package com.eshop.schedule.domain;


import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;


/**
 * @author Elvis
 * @version 1.0
 * @description: 发货明细
 * @date 2022-04-17
 */
@Data
public class ScheduleOrderSendOutDetailDO extends AbstractObject {

	/**
	 * id
	 */
	private Long id;
	/**
	 * 订单id
	 */
	private Long orderInfoId;
	/**
	 * 订单条目id
	 */
	private Long orderItemId;
	/**
	 * 货位库存明细id
	 */
	private Long goodsAllocationStockDetailId;
	/**
	 * 发货数量
	 */
	private Long sendOutCount;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;

}
