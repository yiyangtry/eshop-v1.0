package com.eshop.product.service;

import com.eshop.product.domain.GoodsDTO;
import com.eshop.product.domain.GoodsSkuDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品中心对外提供的接口
 * @date 2022-04-16
 */
public interface CommodityService {

    /**
     * 根据id查询商品sku
     * @param goodsSkuId 商品sku id
     * @return 商品sku DTO
     */
    GoodsSkuDTO getGoodsSkuById(Long goodsSkuId);

    /**
     * 根据id查询商品
     * @param goodsId 商品id
     * @return 商品
     */
    GoodsDTO getGoodsById(Long goodsId);

}
