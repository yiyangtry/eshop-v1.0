package com.eshop.product.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品sku的销售属性值
 * @date 2022-04-16
 */
@Data
public class GoodsSkuSalePropertyValueDTO extends AbstractObject {

        /**
         * id
         */
        private Long id;
        /**
         * 商品sku id
         */
        private Long goodsSkuId;
        /**
         * 类目与属性关联关系id
         */
        private Long relationId;
        /**
         * 属性值
         */
        private String propertyValue;
        /**
         * 创建时间
         */
        private Date gmtCreate;
        /**
         * 修改时间
         */
        private Date gmtModified;

    }