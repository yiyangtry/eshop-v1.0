eshop-v1.0（电商系统代码dev-1.0）

 1、项目架构 

SpringCloud + Dubbo + Nacos2.0.4 + sentinel + seata1.3.0 + rocketmq + redisson

 2、项目服务说明 

| 服务名          | 端口号 | 说明       |
| --------------- | ------ | ---------- |
| eshop-gateway   | 9000   | 网关服务   |
| eshop-inventory | 8001   | 库存服务   |
| eshop-product   | 8002   | 商品服务   |
| eshop-market    | 8003   | 营销服务   |
| eshop-order     | 8005   | 订单服务   |
| eshop-pay       | 8006   | 支付服务   |
| eshop-fulfill   | 8007   | 履约服务   |
| eshop-wms       | 8008   | 仓储服务   |
| eshop-tms       | 8009   | 物流系统   |
| eshop-risk      | 8010   | 风控服务   |
| eshop-customer  | 8011   | 客服服务   |
| eshop-address   | 8012   | 地址服务   |
| eshop-auth      | 8081   | 用户中心 |
| eshop-purchase | 8082 | 采购服务 |



3.swagger接口文档测试：



用户中心接口api文档:



http://127.0.0.1:8081/swagger-ui.html

http://127.0.0.1:8081/doc.html











































