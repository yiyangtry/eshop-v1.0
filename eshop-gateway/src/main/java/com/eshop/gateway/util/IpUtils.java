package com.eshop.gateway.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-10
 */
@Slf4j
public class IpUtils {

    private final static String IP_UTILS_FLAG = ",";

    // 未知IP
    private final static String UNKNOWN = "unknown";

    // 本地 IP
    private final static String LOCALHOST_IP = "0:0:0:0:0:0:0:1";

    private final static String LOCALHOST_IP1 = "127.0.0.1";

    @Deprecated
    public static String getIpAddr(ServerHttpRequest request) {

        // 根据 HttpHeaders 获取 请求 IP地址
        String clientIp = request.getHeaders().getFirst("X-Forwarded-For");
        if (StringUtils.isEmpty(clientIp) || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("x-forwarded-for");
            if (clientIp != null && clientIp.length() != 0 && !UNKNOWN.equalsIgnoreCase(clientIp)) {
                // 多次反向代理后会有多个ip值，第一个ip才是真实ip
                if (clientIp.contains(IP_UTILS_FLAG)) {
                    clientIp = clientIp.split(IP_UTILS_FLAG)[0];
                }
            }
        }
        if (clientIp == null || clientIp.length() == 0 || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("Proxy-Client-IP");
        }
        if (clientIp == null || clientIp.length() == 0 || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("WL-Proxy-Client-IP");
        }
        if (clientIp == null || clientIp.length() == 0 || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("HTTP_CLIENT_IP");
        }
        if (clientIp == null || clientIp.length() == 0 || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("HTTP_X_FORWARDED_FOR");
        }
        if (clientIp == null || clientIp.length() == 0 || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeaders().getFirst("X-Real-IP");
        }

        if (clientIp != null && clientIp.contains("unknown")) {
            clientIp = clientIp.replaceAll("unknown,", "");
            clientIp = clientIp.trim();
        }
        if ("".equals(clientIp) || null == clientIp || "0:0:0:0:0:0:0:1".equals(clientIp) || "0:0:0:0:0:0:0:1%0".equals(clientIp)) {
            clientIp = "127.0.0.1";
        }

        /*
         * 对于获取到多ip的情况下，找到公网ip.
         */
        String sIP = null;
        if (clientIp != null && !clientIp.contains("unknown") && clientIp.indexOf(",") > 0) {
            String[] ipsz = clientIp.split(",");
            for (String anIpsz : ipsz) {
                String ips =null;
                if("0:0:0:0:0:0:0:1%0".equals(anIpsz.trim()) || "".equals(anIpsz.trim()) || null == anIpsz.trim() || "0:0:0:0:0:0:0:1".equals(anIpsz.trim())){
                    ips =  "127.0.0.1";
                }else{
                    ips=anIpsz;
                }
                if (!isInnerIP(ips.trim())) {
                    sIP = ips.trim();
                    break;
                }
            }
            /*
             * 如果多ip都是内网ip，则取第一个ip.
             */
            if (null == sIP) {
                sIP = ipsz[0].trim();
            }
            clientIp = sIP;
        }

        //兼容k8s集群获取ip
        if (StringUtils.isEmpty(clientIp) || UNKNOWN.equalsIgnoreCase(clientIp)) {
            clientIp = request.getRemoteAddress().getAddress().getHostAddress();
            if (LOCALHOST_IP1.equalsIgnoreCase(clientIp) || LOCALHOST_IP.equalsIgnoreCase(clientIp)) {
                //根据网卡取本机配置的IP
                InetAddress iNet = null;
                try {
                    iNet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    log.error("getClientIp error: ", e);
                }
                clientIp = iNet.getHostAddress();
            }
        }
        return clientIp;
    }

    /**
     * 判断IP是否是内网地址
     *
     * @param ipAddress ip地址
     * @return 是否是内网地址
     */
    public static boolean isInnerIP(String ipAddress) {
        boolean isInnerIp;
        long ipNum = getIpNum(ipAddress.replace(",", ""));
        /**
         * 私有IP：A类  10.0.0.0-10.255.255.255
         *  B类  172.16.0.0-172.31.255.255
         *  C类  192.168.0.0-192.168.255.255
         *  当然，还有127这个网段是环回地址
         */
        long aBegin = getIpNum("10.0.0.0");
        long aEnd = getIpNum("10.255.255.255");

        long bBegin = getIpNum("172.16.0.0");
        long bEnd = getIpNum("172.31.255.255");

        long cBegin = getIpNum("192.168.0.0");
        long cEnd = getIpNum("192.168.255.255");
        isInnerIp = isInner(ipNum, aBegin, aEnd) || isInner(ipNum, bBegin, bEnd) || isInner(ipNum, cBegin, cEnd)
                || ipAddress.equals("127.0.0.1");
        return isInnerIp;
    }

    private static long getIpNum(String ipAddress) {
        String[] ip = ipAddress.split("\\.");
        long a = Integer.parseInt(String.valueOf(ip[0]));
        long b = Integer.parseInt(String.valueOf(ip[1]));
        long c = Integer.parseInt(String.valueOf(ip[2]));
        long d = Integer.parseInt(String.valueOf(ip[3]));

        return a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;
    }

    private static boolean isInner(long userIp, long begin, long end) {
        return (userIp >= begin) && (userIp <= end);
    }

    public static String getRealIP(HttpServletRequest request) {
        // 获取客户端ip地址
        String clientIp = request.getHeader("x-forwarded-for");

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getRemoteAddr();
        }

        String[] clientIps = clientIp.split(",");
        if (clientIps.length <= 1) return clientIp.trim();

        // 判断是否来自CDN
        if (isComefromCDN(request)) {
            if (clientIps.length >= 2) return clientIps[clientIps.length - 2].trim();
        }

        return clientIps[clientIps.length - 1].trim();
    }

    private static boolean isComefromCDN(HttpServletRequest request) {
        String host = request.getHeader("host");
        return host.contains("www.189.cn") || host.contains("shouji.189.cn") || host.contains(
                "image2.chinatelecom-ec.com") || host.contains(
                "image1.chinatelecom-ec.com");
    }

}
