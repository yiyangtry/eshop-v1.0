package com.eshop.tms.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流中心接口
 * @date 2022-04-14
 */
@RestController
@RequestMapping("/logistics")
public class LogisticsController {

}
