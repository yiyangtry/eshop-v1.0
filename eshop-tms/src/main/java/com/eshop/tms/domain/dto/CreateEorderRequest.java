package com.eshop.tms.domain.dto;

import lombok.Data;
import java.util.List;


/**
 * @author Elvis
 * @version 1.0
 * @description: 创建电子面单的请求
 * @date 2022-04-14
 */
@Data
public class CreateEorderRequest {

    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 运费
     */
    private Double freight;
    /**
     * 收件人信息
     */
    private Receiver receiver;
    /**
     * 商品信息
     */
    private List<Goods> goodsList;
    /**
     * 商品总毛重
     */
    private Double totalGrossWeight;
    /**
     * 商品购买总件数
     */
    private Long totalPurchaseQuantity;
    /**
     * 商品总体积
     */
    private Double totalVolume;

    /**
     * 收件人信息
     *
     * @author Elvis
     */
    @Data
    public static class Receiver {

        /**
         * 收件人姓名
         */
        private String consignee;
        /**
         * 收件人电话号码
         */
        private String consigneeCellPhoneNumber;
        /**
         * 收货地址
         */
        private String deliveryAddress;
    }
        /**
         * 商品信息
         *
         * @author Elvis
         */
        @Data
        public static class Goods {

            /**
             * 商品名称
             */
            private String goodsName;
            /**
             * 购买数量
             */
            private Long purchaseQuantity;
            /**
             * 商品毛重
             */
            private Double grossWeight;

        }
    }