package com.eshop.tms.domain.dto;

import lombok.Data;

/**
 * @author Elvis
 * @version 1.0
 * @description: 查询物流进度的请求
 * @date 2022-04-14
 */
@Data
public class QueryProgressRequest {

    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 物流单号
     */
    private String logisticCode;


}
