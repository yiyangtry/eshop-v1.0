package com.eshop.tms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板
 * @date 2022-04-14
 */
@Data
public class FreightTemplateDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 运费模板的名称
     */
    private String name;
    /**
     * 运费模板的类型
     */
    private Integer type;
    /**
     * 运费模板的规则
     */
    private String rule;
    /**
     * 运费模板的备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
