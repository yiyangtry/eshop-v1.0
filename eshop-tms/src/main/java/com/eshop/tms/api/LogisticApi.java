package com.eshop.tms.api;

import com.eshop.tms.domain.dto.CreateEorderRequest;
import com.eshop.tms.domain.dto.QueryProgressRequest;
import com.eshop.tms.domain.vo.CreateEorderResponse;
import com.eshop.tms.domain.vo.QueryProgressResponse;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流接口代理：代理了远程的第三方厂商的所有api接口
 * @date 2022-04-14
 */
public interface LogisticApi {

    /**
     * 创建电子面单
     * @param request 请求
     * @return 响应
     * @throws Exception
     */
    CreateEorderResponse createEOrder(CreateEorderRequest request) throws Exception;

    /**
     * 查询物流进度
     * @param request 请求
     * @return 响应
     * @throws Exception
     */
    QueryProgressResponse queryProgress(QueryProgressRequest request) throws Exception;
}
