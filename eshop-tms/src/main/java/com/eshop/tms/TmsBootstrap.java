package com.eshop.tms;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流中心服务
 * @date 2022-04-14
 */
@EnableSwagger2Doc
@EnableSwaggerBootstrapUI
@SpringBootApplication
@EnableDiscoveryClient
public class TmsBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(TmsBootstrap.class, args);
    }

}
