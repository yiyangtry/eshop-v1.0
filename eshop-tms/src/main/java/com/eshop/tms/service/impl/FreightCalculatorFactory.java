package com.eshop.tms.service.impl;

import com.eshop.tms.constant.FreightTemplateType;
import com.eshop.tms.domain.dto.FreightTemplateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费计算器工厂
 * @date 2022-04-14
 */
@Component
public class FreightCalculatorFactory {

	/**
	 * 固定运费计算器
	 */
	@Autowired
	private FixedFreightCalculator fixedFreightCalculator;
	/**
	 * 满X元包邮运费计算器
	 */
	@Autowired
	private ReachFreeFreightCalculator reachFreeFreightCalculator;
	/**
	 * 自定义规则运费计算器
	 */
	@Autowired
	private CustomRuleFreightCalculator customRuleFreightCalculator;
	/**
	 * 默认运费计算器
	 */
	@Autowired
	private DefaultLogisticsFreightCalculator defaultFreightCalculator;
	
	/**
	 * 获取运费模板对应的运费计算器
	 * @param freightTemplate 运费模板
	 * @return 运费计算器
	 */
	public FreightCalculator get(FreightTemplateDTO freightTemplate) {
		if(FreightTemplateType.FIXED.equals(freightTemplate.getType())) {
			return fixedFreightCalculator;
		} else if(FreightTemplateType.REACH_FREE.equals(freightTemplate.getType())) {
			return reachFreeFreightCalculator;
		} else if(FreightTemplateType.CUSTOM_RULE.equals(freightTemplate.getType())) {
			return customRuleFreightCalculator;
		}
		return defaultFreightCalculator;
	}
	
}
