package com.eshop.tms.service.impl;

import com.eshop.common.util.DateProvider;
import com.eshop.order.domain.dto.OrderInfoDTO;
import com.eshop.order.domain.dto.OrderItemDTO;
import com.eshop.product.domain.GoodsDTO;
import com.eshop.product.service.CommodityService;
import com.eshop.tms.api.CreateEorderRequestBuilder;
import com.eshop.tms.api.LogisticApi;
import com.eshop.tms.domain.dto.CreateEorderRequest;
import com.eshop.tms.domain.dto.FreightTemplateDTO;
import com.eshop.tms.domain.vo.CreateEorderResponse;
import com.eshop.tms.service.FreightTemplateService;
import com.eshop.tms.service.LogisticsService;
import com.eshop.wms.domain.dto.LogisticOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流中心接口
 * @date 2022-04-14
 */
@Service
@Slf4j
public class LogisticsServiceImpl implements LogisticsService {

	/**
	 * 日期辅助组件
	 */
	@Autowired
	private DateProvider dateProvider;
	/**
	 * 商品中心接口
	 */
	@Autowired
	private CommodityService commodityService;
	/**
	 * 运费模板管理service组件
	 */
	@Autowired
	private FreightTemplateService freightTemplateService;
	/**
	 * 运费计算器工厂
	 */
	@Autowired
	private FreightCalculatorFactory freightCalculatorFactory;
	/**
	 * 物流api接口
	 */
	@Autowired
	private LogisticApi logisticApi;

	/**
	 * 计算商品sku的运费
	 * @param orderItem 商品sku DTO
	 * @return 商品sku的运费
	 */
	@Override
	public Double calculateFreight(OrderInfoDTO order, OrderItemDTO orderItem) {
		try {
			// 获取商品对应的运费模板
			Long goodsId = orderItem.getGoodsId();
			GoodsDTO goods = commodityService.getGoodsById(goodsId);
			FreightTemplateDTO freightTemplate = freightTemplateService.getById(
					goods.getFreightTemplateId());
			
			// 获取运费计算器
			FreightCalculator freightCalculator = freightCalculatorFactory.get(freightTemplate);
			Double freight = freightCalculator.calculate(freightTemplate, order, orderItem);
			
			return freight;
		} catch (Exception e) {
			log.error("error", e);
			return 0.0;
		}
	}
	
	/**
	 * 申请物流单
	 * @param order 订单
	 * @return 物流单
	 */
	@Override
	public LogisticOrderDTO applyLogisticOrder(OrderInfoDTO order) {
		try {
			CreateEorderRequest request = CreateEorderRequestBuilder.get()
					.buildOrderRelatedInfo(order)
					.buildReceiver(order) 
					.buildGoodsList(order)
					.buildTotalDataMetric(order)
					.create();
			
			CreateEorderResponse response = logisticApi.createEOrder(request);
			
			LogisticOrderDTO logisticOrder = createLogisticOrder(response);
 			
			return logisticOrder;
		} catch (Exception e) {
			log.error("error", e);
			return null;
		}
	}
	
	/**
	 * 创建物流单
	 * @param response 创建电子面单响应结果
	 * @return 物流单
	 * @throws Exception
	 */
	private LogisticOrderDTO createLogisticOrder(
			CreateEorderResponse response) throws Exception {
		LogisticOrderDTO logisticOrder = new LogisticOrderDTO();
		logisticOrder.setLogisticCode(response.getLogisticCode());
		logisticOrder.setContent(response.getLogisticOrderContent()); 
		logisticOrder.setGmtCreate(dateProvider.getCurrentTime()); 
		logisticOrder.setGmtModified(dateProvider.getCurrentTime()); 
		
		return logisticOrder;
	}
	
	/**
	 * 获取订单的签收时间
	 * @param orderId 订单id
	 * @param orderNo 订单编号
	 * @return 签收时间
	 */
	@Override
	public Date getSignedTime(Long orderId, String orderNo) {
		try {
			return dateProvider.getCurrentTime();
		} catch (Exception e) {
			log.error("error", e);
			return null;
		}
	}
	
}
