package com.eshop.tms.service.impl;


import com.eshop.order.domain.dto.OrderInfoDTO;
import com.eshop.order.domain.dto.OrderItemDTO;
import com.eshop.tms.domain.dto.FreightTemplateDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费计算器接口
 * @date 2022-04-14
 */
public interface FreightCalculator {

	/**
	 * 计算订单条目的运费
	 * @param freightTemplate 运费模板
	 * @param order 订单
	 * @param orderItem 订单条目
	 * @return 运费
	 * @throws Exception
	 */
	Double calculate(FreightTemplateDTO freightTemplate,
					 OrderInfoDTO order, OrderItemDTO orderItem) throws Exception;
	
}
