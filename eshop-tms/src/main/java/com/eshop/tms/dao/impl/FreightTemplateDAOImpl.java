package com.eshop.tms.dao.impl;

import com.eshop.common.util.DateProvider;
import com.eshop.tms.dao.FreightTemplateDAO;
import com.eshop.tms.domain.FreightTemplateDO;
import com.eshop.tms.mapper.FreightTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板管理DAO组件
 * @date 2022-04-14
 */
@Repository
public class FreightTemplateDAOImpl implements FreightTemplateDAO {

    /**
     * 运费模板管理mapper组件
     */
    @Autowired
    private FreightTemplateMapper freightTemplateMapper;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 新增运费模板
     * @param freightTemplate 运费模板
     */
    @Override
    public void save(FreightTemplateDO freightTemplate) throws Exception {
        freightTemplate.setGmtCreate(dateProvider.getCurrentTime());
        freightTemplate.setGmtModified(dateProvider.getCurrentTime());
        freightTemplateMapper.save(freightTemplate);
    }
}
