package com.eshop.tms.dao;

import com.eshop.tms.domain.FreightTemplateDO;
import com.eshop.tms.domain.dto.FreightTemplateQuery;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板管理DAO接口
 * @date 2022-04-14
 */
public interface FreightTemplateDAO {

    /**
     * 新增运费模板
     * @param freightTemplate 运费模板
     * @throws Exception
     */
    void save(FreightTemplateDO freightTemplate) throws Exception;

    /**
     * 分页查询运费模板
     * @param query 运费模板查询条件
     * @return 运费模板
     * @throws Exception
     */
    List<FreightTemplateDO> listByPage(FreightTemplateQuery query) throws Exception;

    /**
     * 根据id查询运费模板
     * @param id 运费模板id
     * @return 运费模板
     * @throws Exception
     */
    FreightTemplateDO getById(Long id) throws Exception;

    /**
     * 更新运费模板
     * @param freightTemplate 运费模板
     * @throws Exception
     */
    void update(FreightTemplateDO freightTemplate) throws Exception;

}
