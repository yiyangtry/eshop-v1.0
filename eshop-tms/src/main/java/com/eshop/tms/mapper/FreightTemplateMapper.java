package com.eshop.tms.mapper;


import com.eshop.tms.domain.FreightTemplateDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板管理mapper组件
 * @date 2022-04-14
 */
@Mapper
public interface FreightTemplateMapper {

    /**
     * 新增运费模板
     * @param freightTemplate 运费模板
     */
    @Insert("INSERT INTO logistics_freight_template("
            + "name,"
            + "type,"
            + "rule,"
            + "remark,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{name},"
            + "#{type},"
            + "#{rule},"
            + "#{remark},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(FreightTemplateDO freightTemplate);
}
