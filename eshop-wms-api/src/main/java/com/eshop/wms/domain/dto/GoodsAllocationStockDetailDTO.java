package com.eshop.wms.domain.dto;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 货位库存明细
 * @date 2022-04-15
 */
@Data
public class GoodsAllocationStockDetailDTO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 货位id
     */
    private Long goodsAllocationId;
    /**
     * 这一批商品的上架时间
     */
    private Date putOnTime;
    /**
     * 这一批商品本次上架的数量
     */
    private Long putOnQuantity;
    /**
     * 这一批上架的商品当前还剩余的库存数量
     */
    private Long currentStockQuantity;
    /**
     * 锁定库存
     */
    private Long lockedStockQuantity;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}