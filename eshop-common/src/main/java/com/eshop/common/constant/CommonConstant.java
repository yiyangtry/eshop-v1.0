package com.eshop.common.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-10
 */
public class CommonConstant {

    public static final String JWT_KEY_USER_ID = "userId";
    public static final String JWT_KEY_NAME = "name";
    public static final String JWT_KEY_USER_NAME = "userName";
}
