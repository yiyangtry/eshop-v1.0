package com.eshop.common.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 集合大小常量类
 * @date 2022-04-15
 */
public class CollectionSize {

    /**
     * 默认集合大小
     */
    public static final Integer DEFAULT = 100;

    private CollectionSize() {

    }

}
