package com.eshop.common.json;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Elvis
 * @version 1.0
 * @description: json表达式上下文
 * @date 2022-04-16
 */
public class JsonExpressionContext {

	/**
	 * 目标json
	 */
	private JSONObject targetJson;
	/**
	 * 当前节点
	 */
	private JSONObject currentJsonNode;
	
	public JsonExpressionContext(JSONObject targetJson) {
		this.targetJson = targetJson;
	}
	
	public JSONObject getCurrentJsonNode() {
		return currentJsonNode;
	}
	public void setCurrentJsonNode(JSONObject currentJsonNode) {
		this.currentJsonNode = currentJsonNode;
	}
	public JSONObject getTargetJson() {
		return targetJson;
	}
	
}
