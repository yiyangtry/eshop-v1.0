package com.eshop.common.json;

/**
 * @author Elvis
 * @version 1.0
 * @description: json表达式接口
 * @date 2022-04-16
 */
public interface JsonExpression {

	/**
	 * 解释表达式
	 * @param context 上下文
	 * @return 结果
	 * @throws Exception
	 */
	Object interpret(JsonExpressionContext context) throws Exception;
	
}
