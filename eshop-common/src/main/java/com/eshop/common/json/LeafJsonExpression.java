package com.eshop.common.json;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Elvis
 * @version 1.0
 * @description: 叶子表达式
 * @date 2022-04-16
 */
public class LeafJsonExpression implements JsonExpression {

	private String jsonNodeName;
	
	public LeafJsonExpression(String jsonNodeName) {
		this.jsonNodeName = jsonNodeName;
	}
	
	@Override
	public Object interpret(JsonExpressionContext context) throws Exception {
		JSONObject currentJsonNode = context.getCurrentJsonNode();
		if(currentJsonNode == null) {
			return context.getTargetJson().get(jsonNodeName);
		} else {
			return currentJsonNode.get(jsonNodeName);
		}
	}
	
}
