package com.eshop.common.json;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Elvis
 * @version 1.0
 * @description: 用于组合别的表达式的json表达式
 * @date 2022-04-16
 */
public class CompositeJsonExpression implements JsonExpression {

	/**
	 * json节点名称
	 */
	private String jsonNodeName;
	/**
	 * 子表达式
	 */
	private JsonExpression childJsonExpression;
	
	public CompositeJsonExpression(String jsonNodeName) {
		this.jsonNodeName = jsonNodeName;
	}
	
	@Override
	public Object interpret(JsonExpressionContext context) throws Exception {
		JSONObject currentJsonNode = context.getCurrentJsonNode();
		
		if(currentJsonNode == null) {
			JSONObject targetJson = context.getTargetJson();
			currentJsonNode = targetJson.getJSONObject(jsonNodeName);
			context.setCurrentJsonNode(currentJsonNode); 
		} else {
			currentJsonNode = currentJsonNode.getJSONObject(jsonNodeName);
			context.setCurrentJsonNode(currentJsonNode); 
		}
		
		return childJsonExpression.interpret(context); 
	}

	public JsonExpression getChildJsonExpression() {
		return childJsonExpression;
	}
	public void setChildJsonExpression(JsonExpression childJsonExpression) {
		this.childJsonExpression = childJsonExpression;
	}
	
}
