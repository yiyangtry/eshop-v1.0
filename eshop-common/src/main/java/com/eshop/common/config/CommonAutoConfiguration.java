package com.eshop.common.config;

import com.eshop.common.bean.SpringApplicationContext;
import com.eshop.common.web.WebConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Elvis
 * @version 1.0
 * @description: spring容器组件
 * @date 2022-04-10
 */
@Configuration
@Import(value = {WebConfiguration.class, SpringApplicationContext.class})
public class CommonAutoConfiguration {

}
