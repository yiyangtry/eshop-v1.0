package com.eshop.common.util;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-10
 */
public class StringHelper {
    public static String getObjectValue(Object obj) {
        return obj == null ? "" : obj.toString();
    }
}
