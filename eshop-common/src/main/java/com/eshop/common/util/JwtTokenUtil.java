package com.eshop.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Elvis
 * @version 1.0
 * @description: JWT工具类
 * @date 2022-04-10
 */
@Component
@Slf4j
public class JwtTokenUtil {

    /**
     * 过期时间，30分钟
     */
    private static int expire = 1000 * 60 * 30;

    private static byte[] priKey;

    private static byte[] pubKey;

    static {
        try {
            priKey = RsaKeyHelper.generatePrivateKey("wADxWwW1YW7OTNdq#^$");
            pubKey = RsaKeyHelper.generatePublicKey("wADxWwW1YW7OTNdq#^$");
        } catch (Exception e) {
            log.error("生成公钥密钥异常：{}" + e.getMessage());
        }
    }

    public static String generateToken(IJWTInfo jwtInfo) throws Exception {
        return JWTHelper.generateToken(jwtInfo, priKey, expire);
    }

    public static IJWTInfo getInfoFromToken(String token) throws Exception {
        return JWTHelper.getInfoFromToken(token, pubKey);
    }

}
