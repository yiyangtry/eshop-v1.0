package com.eshop.common.util;

/**
 * @author Elvis
 * @version 1.0
 * @description: 领域模型对象的类型
 * @date 2022-04-10
 */
public class DomainType {


    /**
     * VO：Value Object
     */
    public static final String VO = "VO";
    /**
     * DTO：Data Transfer Object
     */
    public static final String DTO = "DTO";
    /**
     * DO：Data Object
     */
    public static final String DO = "DO";

    private DomainType() {

    }
}