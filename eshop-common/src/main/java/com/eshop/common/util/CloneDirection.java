package com.eshop.common.util;

/**
 * @author Elvis
 * @version 1.0
 * @description: 克隆方向
 * @date 2022-04-10
 */
public class CloneDirection {

    /**
     * 正向克隆：从VO->DTO，DTO->DO
     */
    public static final Integer FORWARD = 1;
    /**
     * 反向克隆：从DO->DTO，DTO->VO
     */
    public static final Integer OPPOSITE = 2;

    private CloneDirection() {

    }

}
