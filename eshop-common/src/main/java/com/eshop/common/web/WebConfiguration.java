package com.eshop.common.web;

import com.eshop.common.util.DateProvider;
import com.eshop.common.util.DateProviderImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Elvis
 * @version 1.0
 * @description: web相关bean组件配置
 * @date 2022-04-10
 */
@Configuration
public class WebConfiguration {

    @Bean
    public DateProvider dateProvider() {
        return new DateProviderImpl();
    }

}
