package com.eshop.common.exception;

/**
 * @author Elvis
 * @version 1.0
 * @description: 异常错误码枚举抽象定义
 * @date 2022-04-05
 */
public interface BaseErrorCodeEnum {

    String getErrorCode();

    String getErrorMsg();
}
