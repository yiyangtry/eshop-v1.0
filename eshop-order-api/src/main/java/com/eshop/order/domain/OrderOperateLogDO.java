package com.eshop.order.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单操作日志
 * @date 2022-05-02
 */
@Data
public class OrderOperateLogDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 订单id
     */
    private Long orderInfoId;
    /**
     * 操作类型
     */
    private Integer operateType;
    /**
     * 操作内容
     */
    private String operateContent;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
