package com.eshop.order.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 退货申请
 * @date 2022-05-02
 */
@Data
public class ReturnGoodsApplyDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 订单id
     */
    private Long orderInfoId;
    /**
     * 退货原因
     */
    private Integer returnGoodsReason;
    /**
     * 退货备注
     */
    private String returnGoodsComment;
    /**
     * 退货申请状态
     */
    private Integer returnGoodsApplyStatus;
    /**
     * 退货物流单号
     */
    private String returnGoodsLogisticCode;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
