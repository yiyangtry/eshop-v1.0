package com.eshop.inventory.async;

import lombok.Data;

/**
 * 商品库存更新消息
 * @author zhonghuashishan
 *
 */
@Data
public class StockUpdateMessage {

	/**
	 * id
	 */
	private String id;
	/**
	 * 商品库存更新操作
	 */
	private Integer operation;
	/**
	 * 核心参数数据
	 */
	private Object parameter;
}
