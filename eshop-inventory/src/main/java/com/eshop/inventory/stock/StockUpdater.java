package com.eshop.inventory.stock;


/**
 * @author Elvis
 * @version 1.0
 * @description: 商品库存更新命令的接口
 * @date 2022-04-16
 */
public interface StockUpdater {

	/**
	 * 更新商品库存
	 * @return 处理结果
	 */
	Boolean updateGoodsStock();
	
}
