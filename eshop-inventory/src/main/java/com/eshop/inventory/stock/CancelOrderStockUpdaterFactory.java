package com.eshop.inventory.stock;


import com.eshop.common.constant.CollectionSize;
import com.eshop.common.util.DateProvider;
import com.eshop.inventory.dao.GoodsStockDAO;
import com.eshop.inventory.domain.GoodsStockDO;
import com.eshop.order.domain.dto.OrderInfoDTO;
import com.eshop.order.domain.dto.OrderItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Elvis
 * @version 1.0
 * @description: 取消订单库存更新组件工厂
 * @date 2022-04-16
 */
@Component
public class CancelOrderStockUpdaterFactory<T> 
		extends AbstractStockUpdaterFactory<T> {

	/**
	 * 构造函数
	 * @param goodsStockDAO 商品库存管理模块DAO组件
	 * @param dateProvider 日期辅助组件
	 */
	@Autowired
	public CancelOrderStockUpdaterFactory(
			GoodsStockDAO goodsStockDAO,
			DateProvider dateProvider) {
		super(goodsStockDAO, dateProvider);
	}
	
	/** 
	 * 获取要更新库存的商品sku id的集合
	 */
	@Override
	protected List<Long> getGoodsSkuIds(T parameter) throws Exception {
		OrderInfoDTO orderInfoDTO = (OrderInfoDTO) parameter;
		
		List<Long> goodsSkuIds = new ArrayList<Long>();
		
		List<OrderItemDTO> orderItemDTOs = orderInfoDTO.getOrderItems();
		for(OrderItemDTO orderItemDTO : orderItemDTOs) {
			goodsSkuIds.add(orderItemDTO.getGoodsSkuId());
		}
		
		return goodsSkuIds;
	}

	/**
	 * 创建商品库存更新组件
	 * @param goodsStockDOs 商品库存DO对象集合
	 * @param parameter 订单DTO对象
	 * @return 商品库存更新组件
	 */
	@Override
	protected StockUpdater create(List<GoodsStockDO> goodsStockDOs,
			T parameter) throws Exception {
		OrderInfoDTO orderInfoDTO = (OrderInfoDTO) parameter;
		
		Map<Long, OrderItemDTO> orderItemDTOMap = new HashMap<Long, OrderItemDTO>(CollectionSize.DEFAULT);
		for(OrderItemDTO orderItemDTO : orderInfoDTO.getOrderItems()) {
			orderItemDTOMap.put(orderItemDTO.getGoodsSkuId(), orderItemDTO);
		}
		
		return new CancelOrderStockUpdater(goodsStockDOs, goodsStockDAO, 
				dateProvider, orderItemDTOMap);
	} 

}
