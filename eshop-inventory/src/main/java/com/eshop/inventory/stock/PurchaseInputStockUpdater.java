package com.eshop.inventory.stock;



import com.eshop.common.util.DateProvider;
import com.eshop.inventory.dao.GoodsStockDAO;
import com.eshop.inventory.domain.GoodsStockDO;
import com.eshop.wms.domain.dto.PurchaseInputOrderItemDTO;

import java.util.List;
import java.util.Map;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购入库库存更新命令
 * @date 2022-04-16
 */
public class PurchaseInputStockUpdater extends AbstractStockUpdater {

	/**
	 * 采购入库单条目DTO集合
	 */
	private Map<Long, PurchaseInputOrderItemDTO> purcahseInputOrderItemDTOMap;
	
	/**
	 * 构造函数
	 * @param goodsStockDO 商品库存DO对象
	 * @param goodsStockDAO 商品库存管理模块的DAO组件
	 * @param dateProvider 日期辅助组件
	 */
	public PurchaseInputStockUpdater(
			List<GoodsStockDO> goodsStockDOs,
			GoodsStockDAO goodsStockDAO,
			DateProvider dateProvider,
			Map<Long, PurchaseInputOrderItemDTO> purcahseInputOrderItemDTOMap) {
		super(goodsStockDOs, goodsStockDAO, dateProvider);
		this.purcahseInputOrderItemDTOMap = purcahseInputOrderItemDTOMap;
	}
	
	/**
	 * 更新销售库存
	 */
	@Override
	protected void updateSaleStockQuantity() throws Exception {
		for(GoodsStockDO goodsStockDO : goodsStockDOs) {
			PurchaseInputOrderItemDTO purchaseInputOrderItemDTO = 
					purcahseInputOrderItemDTOMap.get(goodsStockDO.getGoodsSkuId());
			goodsStockDO.setSaleStockQuantity(goodsStockDO.getSaleStockQuantity() 
					+ purchaseInputOrderItemDTO.getArrivalCount()); 
		}
	}

	/**
	 * 更新锁定库存
	 */
	@Override
	protected void updateLockedStockQuantity() throws Exception {
		
	}

	/**
	 * 更新已销售库存
	 */
	@Override
	protected void updateSaledStockQuantity() throws Exception {
		
	}

}
