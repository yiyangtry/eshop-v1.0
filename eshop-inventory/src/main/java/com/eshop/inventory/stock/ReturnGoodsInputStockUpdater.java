package com.eshop.inventory.stock;

import com.eshop.common.util.DateProvider;
import com.eshop.inventory.dao.GoodsStockDAO;
import com.eshop.inventory.domain.GoodsStockDO;
import com.eshop.wms.domain.dto.ReturnGoodsInputOrderItemDTO;

import java.util.List;
import java.util.Map;


/**
 * @author Elvis
 * @version 1.0
 * @description: 退货入库
 * @date 2022-04-16
 */
public class ReturnGoodsInputStockUpdater extends AbstractStockUpdater {

	/**
	 * 退货入库单条目DTO集合
	 */
	private Map<Long, ReturnGoodsInputOrderItemDTO> returnGoodsInputOrderItemDTOMap;
	
	/**
	 * 构造函数
	 * @param goodsStockDOs 商品库存DO集合
	 * @param goodsStockDAO 商品库存管理模块的DAO组件
	 * @param dateProvider 日期辅助组件
	 * @param returnGoodsInputOrderItemDTOMap 退货入库单条目DTO集合
	 */
	public ReturnGoodsInputStockUpdater(
			List<GoodsStockDO> goodsStockDOs,
			GoodsStockDAO goodsStockDAO,
			DateProvider dateProvider,
			Map<Long, ReturnGoodsInputOrderItemDTO> returnGoodsInputOrderItemDTOMap) {
		super(goodsStockDOs, goodsStockDAO, dateProvider);
		this.returnGoodsInputOrderItemDTOMap = returnGoodsInputOrderItemDTOMap;
	}

	/**
	 * 更新销售库存
	 */
	@Override
	protected void updateSaleStockQuantity() throws Exception {
		for(GoodsStockDO goodsStockDO : goodsStockDOs) {
			ReturnGoodsInputOrderItemDTO returnGoodsInputOrderItemDTO = 
					returnGoodsInputOrderItemDTOMap.get(goodsStockDO.getGoodsSkuId());
			goodsStockDO.setSaleStockQuantity(goodsStockDO.getSaleStockQuantity() 
					+ returnGoodsInputOrderItemDTO.getArrivalCount()); 
		}
	}

	/**
	 * 更新锁定库存
	 */
	@Override
	protected void updateLockedStockQuantity() throws Exception {
		
	}

	/**
	 * 更新已销售库存
	 */
	@Override
	protected void updateSaledStockQuantity() throws Exception {
		for(GoodsStockDO goodsStockDO : goodsStockDOs) {
			ReturnGoodsInputOrderItemDTO returnGoodsInputOrderItemDTO = 
					returnGoodsInputOrderItemDTOMap.get(goodsStockDO.getGoodsSkuId());
			goodsStockDO.setSaledStockQuantity(goodsStockDO.getSaledStockQuantity() 
					- returnGoodsInputOrderItemDTO.getArrivalCount()); 
		}
	}
	
}
