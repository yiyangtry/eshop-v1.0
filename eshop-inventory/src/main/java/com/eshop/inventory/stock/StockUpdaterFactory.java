package com.eshop.inventory.stock;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存更新命令工厂接口
 * @date 2022-04-16
 */
public interface StockUpdaterFactory<T> { 

	/**
	 * 创建一个库存更新命令
	 * @param parameter 参数对象
	 * @return 库存更新命令
	 */
	StockUpdater create(T parameter);
	
}
