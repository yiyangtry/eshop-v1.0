package com.eshop.inventory.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存状态常量类
 * @date 2022-04-16
 */
public class StockStatus {

    /**
     * 有库存
     */
    public static final Integer IN_STOCK = 1;
    /**
     * 无库存
     */
    public static final Integer NOT_IN_STOCK = 0;

}
