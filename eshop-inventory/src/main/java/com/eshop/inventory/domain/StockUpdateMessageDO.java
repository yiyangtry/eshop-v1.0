package com.eshop.inventory.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存更新消息DO类
 * @date 2022-04-12
 */
@Data
public class StockUpdateMessageDO implements Serializable {

    /**
     * id
     */
    private Long id;
    /**
     * 库存更新消息id
     */
    private String messageId;
    /**
     * 库存更新操作
     */
    private Integer operation;
    /**
     * 参数对象：json格式
     */
    private String parameter;
    /**
     * 参数类型
     */
    private String paramterClazz;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
