package com.eshop.inventory.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品sku库存DO类
 * @date 2022-04-12
 */
@Data
public class GoodsStockDO implements Serializable {
    /**
     * id
     */
    private Long id;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 商品sku的销售库存
     */
    private Long saleStockQuantity;
    /**
     * 商品sku的锁定库存
     */
    private Long lockedStockQuantity;
    /**
     * 商品sku的已销售库存
     */
    private Long saledStockQuantity;
    /**
     * 商品sku的库存状态
     */
    private Integer stockStatus;
    /**
     * 商品sku库存的创建时间
     */
    private Date gmtCreate;
    /**
     * 商品sku库存的修改时间
     */
    private Date gmtModified;
}
