package com.eshop.tms.service;


import com.eshop.order.domain.dto.OrderInfoDTO;
import com.eshop.order.domain.dto.OrderItemDTO;
import com.eshop.wms.domain.dto.LogisticOrderDTO;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流中心对外提供的接口
 * @date 2022-04-16
 */
public interface LogisticsService {

	/**
	 * 计算商品sku的运费
	 * @param order 订单
	 * @param orderItem 订单条目
	 * @return 商品sku的运费
	 */
	Double calculateFreight(OrderInfoDTO order, OrderItemDTO orderItem);
	
	/**
	 * 申请物流单
	 * @param order 订单
	 * @return 物流单
	 */
	LogisticOrderDTO applyLogisticOrder(OrderInfoDTO order);
	
	/**
	 * 获取订单的签收时间
	 * @param orderId 订单id
	 * @param orderNo 订单编号
	 * @return 签收时间
	 */
	Date getSignedTime(Long orderId, String orderNo);
	
}
