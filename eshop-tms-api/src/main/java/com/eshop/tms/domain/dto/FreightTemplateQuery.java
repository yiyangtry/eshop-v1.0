package com.eshop.tms.domain.dto;

import lombok.Data;

/**
 * 运费模板查询条件
 * @author zhonghuashishan
 *
 */
@Data
public class FreightTemplateQuery {

	/**
	 * 分页查询起始位置
	 */
	private Integer offset;
	/**
	 * 每页显示的数据条数
	 */
	private Integer size;
	/**
	 * 运费模板名称
	 */
	private String name;
	/**
	 * 运费模板类型
	 */
	private Integer type;

}
