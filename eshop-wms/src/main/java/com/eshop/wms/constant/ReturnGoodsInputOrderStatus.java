package com.eshop.wms.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 退货入库单状态
 * @date 2022-04-15
 */
public class ReturnGoodsInputOrderStatus {

        /**
         * 编辑中
         */
        public static final Integer EDITING = 1;
        /**
         * 待审核
         */
        public static final Integer WAIT_FOR_APPROVE = 2;
        /**
         * 已完成
         */
        public static final Integer FINISHED = 3;

        private ReturnGoodsInputOrderStatus() {

        }
}
