package com.eshop.wms.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 销售出库单审核结果
 * @date 2022-04-15
 */
public class SaleDeliveryOrderApproveResult {

    /**
     * 审核通过
     */
    public static final Integer PASSED = 1;
    /**
     * 审核不通难过
     */
    public static final Integer REJECTED = 2;

    private SaleDeliveryOrderApproveResult() {

    }

}
