package com.eshop.wms.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购入库单审核结果
 * @date 2022-04-15
 */
public class ReturnGoodsInputOrderApproveResult {

    /**
     * 采购入库单审核通过
     */
    public static final Integer PASSED = 1;
    /**
     * 采购入库单审核不通过
     */
    public static final Integer REJECTED = 0;

    private ReturnGoodsInputOrderApproveResult() {

    }

}
