package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购入库单的商品上架条目
 * @date 2022-04-14
 */
@Data
public class PurchaseInputOrderPutOnItemDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 采购入库单条目id
     */
    private Long purchaseInputOrderItemId;
    /**
     * 货位id
     */
    private Long goodsAllocationId;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 商品上架数量
     */
    private Long putOnShelvesCount;
    /**
     * 商品上架条目的创建时间
     */
    private Date gmtCreate;
    /**
     * 商品上架条目的修改时间
     */
    private Date gmtModified;
}
