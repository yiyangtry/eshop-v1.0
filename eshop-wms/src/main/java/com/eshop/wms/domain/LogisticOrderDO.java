package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流单
 * @date 2022-04-14
 */
@Data
public class LogisticOrderDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 销售出库单id
     */
    private Long saleDeliveryOrderId;
    /**
     * 物流单号
     */
    private String logisticCode;
    /**
     * 物流单内容
     */
    private String content;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
