package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 销售出库单条目
 * @date 2022-04-14
 */
@Data
public class SaleDeliveryOrderItemDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 销售出库单id
     */
    private Long saleDeliveryOrderId;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 商品sku编号
     */
    private String goodsSkuCode;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品sku的销售属性
     */
    private String saleProperties;
    /**
     * 商品毛重
     */
    private Double goodsGrossWeight;
    /**
     * 购买数量
     */
    private Long purchaseQuantity;
    /**
     * 购买价格
     */
    private Double purchasePrice;
    /**
     * 使用的促销活动的id
     */
    private Long promotionActivityId;
    /**
     * 商品长度
     */
    private Double goodsLength;
    /**
     * 商品宽度
     */
    private Double goodsWidth;
    /**
     * 商品高度
     */
    private Double goodsHeight;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
