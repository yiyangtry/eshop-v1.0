package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 货位库存
 * @date 2022-04-14
 */
@Data
public class WmsGoodsAllocationStockDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 货位id
     */
    private Long goodsAllocationId;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 可用库存
     */
    private Long availableStockQuantity;
    /**
     * 锁定库存
     */
    private Long lockedStockQuantity;
    /**
     * 已出库库存
     */
    private Long outputStockQuantity;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
