package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 销售出库单发货明细
 * @date 2022-04-14
 */
@Data
public class SaleDeliveryOrderSendOutDetailDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 销售出库单条目id
     */
    private Long saleDeliveryOrderItemId;
    /**
     * 货位库存明细id
     */
    private Long goodsAllocationStockDetailId;
    /**
     * 发货数量
     */
    private Long sendOutCount;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
