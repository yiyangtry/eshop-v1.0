package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购入库单条目
 * @date 2022-04-14
 */
@Data
public class PurchaseInputOrderItemDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 采购入库单id
     */
    private Long purchaseInputOrderId;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 商品sku的采购数量
     */
    private Long purchaseCount;
    /**
     * 商品sku的采购价格
     */
    private Double purchasePrice;
    /**
     * 商品sku到货后质检出来的合格商品数量
     */
    private Long qualifiedCount;
    /**
     * 商品sku实际到货的数量
     */
    private Long arrivalCount;
    /**
     * 采购入库单条目的创建时间
     */
    private Date gmtCreate;
    /**
     * 采购入库单条目的修改时间
     */
    private Date gmtModified;
}
