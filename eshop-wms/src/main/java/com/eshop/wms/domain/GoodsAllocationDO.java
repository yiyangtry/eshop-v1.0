package com.eshop.wms.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 货位
 * @date 2022-04-14
 */
@Data
public class GoodsAllocationDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 货位编号
     */
    private String code;
    /**
     * 货位备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
