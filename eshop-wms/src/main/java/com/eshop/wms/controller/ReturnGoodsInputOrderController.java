package com.eshop.wms.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elvis
 * @version 1.0
 * @description: 退货入库单管理controller组件
 * @date 2022-04-14
 */

@RestController
@RequestMapping("/returnGoodsInputOrder")
public class ReturnGoodsInputOrderController {
}
