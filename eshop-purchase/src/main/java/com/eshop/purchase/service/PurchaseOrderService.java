package com.eshop.purchase.service;

import com.eshop.purchase.domain.dto.PurchaseOrderDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理service接口
 * @date 2022-04-11
 */
public interface PurchaseOrderService {

    /**
     * 新增采购单
     * @param purchaseOrder 采购单
     * @throws Exception
     */
    void save(PurchaseOrderDTO purchaseOrder) throws Exception;
}
