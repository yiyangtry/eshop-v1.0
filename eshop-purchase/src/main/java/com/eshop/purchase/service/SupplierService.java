package com.eshop.purchase.service;

import com.eshop.purchase.domain.dto.SupplierDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理service接口
 * @date 2022-04-12
 */
public interface SupplierService {

    /**
     * 新增供应商
     * @param supplier 供应商
     * @throws Exception
     */
    void save(SupplierDTO supplier) throws Exception;
}
