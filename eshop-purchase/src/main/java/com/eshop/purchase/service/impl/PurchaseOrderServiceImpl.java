package com.eshop.purchase.service.impl;

import com.eshop.common.util.ObjectUtils;
import com.eshop.purchase.constant.PurchaseOrderStatus;
import com.eshop.purchase.dao.PurchaseOrderDAO;
import com.eshop.purchase.dao.PurchaseOrderItemDAO;
import com.eshop.purchase.service.PurchaseOrderService;
import com.eshop.purchase.domain.PurchaseOrderDO;
import com.eshop.purchase.domain.PurchaseOrderItemDO;
import com.eshop.purchase.domain.dto.PurchaseOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理service组件
 * @date 2022-04-12
 */
@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    /**
     * 采购单管理DAO组件
     */
    @Autowired
    private PurchaseOrderDAO purchaseOrderDAO;

    /**
     * 采购单条目管理DAO组件
     */
    @Autowired
    private PurchaseOrderItemDAO purchaseOrderItemDAO;

    /**
     * 新增采购单
     * @param purchaseOrder 采购单
     */
    @Override
    public void save(PurchaseOrderDTO purchaseOrder) throws Exception {
        purchaseOrder.setStatus(PurchaseOrderStatus.EDITING);
        Long purchaseOrderId = purchaseOrderDAO.save(purchaseOrder.clone(
                PurchaseOrderDO.class));
        purchaseOrder.setId(purchaseOrderId);
        batchSavePurchaseOrderItems(purchaseOrder);
    }

    /**
     * 批量新增采购单条目
     * @param purchaseOrder
     * @throws Exception
     */
    private void batchSavePurchaseOrderItems(PurchaseOrderDTO purchaseOrder) throws Exception {
        List<PurchaseOrderItemDO> purchaseOrderItems = ObjectUtils.convertList(
                purchaseOrder.getItems(), PurchaseOrderItemDO.class);
        purchaseOrderItemDAO.batchSave(purchaseOrder.getId(), purchaseOrderItems);
    }
}
