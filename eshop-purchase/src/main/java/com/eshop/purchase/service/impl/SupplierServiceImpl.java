package com.eshop.purchase.service.impl;

import com.eshop.purchase.domain.dto.SupplierDTO;
import com.eshop.purchase.dao.SupplierDAO;
import com.eshop.purchase.domain.SupplierDO;
import com.eshop.purchase.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理service组件
 * @date 2022-04-12
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    /**
     * 供应商管理DAO组件
     */
    @Autowired
    private SupplierDAO supplierDAO;

    /**
     * 新增供应商
     * @param supplier 供应商
     */
    @Override
    public void save(SupplierDTO supplier) throws Exception {
        supplierDAO.save(supplier.clone(SupplierDO.class));
    }
}
