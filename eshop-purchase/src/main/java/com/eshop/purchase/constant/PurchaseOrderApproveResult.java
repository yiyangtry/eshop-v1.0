package com.eshop.purchase.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单审核结果
 * @date 2022-04-11
 */
public class PurchaseOrderApproveResult {

    /**
     * 通过审核
     */
    public static final Integer PASSED = 1;
    /**
     * 未通过审核
     */
    public static final Integer REJECTED = 0;

    private PurchaseOrderApproveResult() {

    }
}
