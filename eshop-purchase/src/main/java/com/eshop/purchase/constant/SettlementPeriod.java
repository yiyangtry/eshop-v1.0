package com.eshop.purchase.constant;

/**
 * @author Elvis
 * @version 1.0
 * @description: 结算周期
 * @date 2022-04-12
 */
public class SettlementPeriod {

    /**
     * 周结算
     */
    public static final Integer WEEK = 1;
    /**
     * 月结算
     */
    public static final Integer MONTH = 2;
    /**
     * 季度结算
     */
    public static final Integer QUARTER = 3;

    private SettlementPeriod() {

    }
}
