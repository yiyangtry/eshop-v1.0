package com.eshop.purchase.dao;

import com.eshop.purchase.domain.SupplierDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理DAO接口
 * @date 2022-04-12
 */
public interface SupplierDAO {

    /**
     * 新增供应商
     * @param supplier 供应商
     * @throws Exception
     */
    void save(SupplierDO supplier) throws Exception;
}
