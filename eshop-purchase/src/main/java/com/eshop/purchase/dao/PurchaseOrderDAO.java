package com.eshop.purchase.dao;

import com.eshop.purchase.domain.PurchaseOrderDO;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理DAO接口
 * @date 2022-04-12
 */
public interface PurchaseOrderDAO {

    /**
     * 新增采购单
     * @param purchaseOrder 采购单
     * @return 采购单id
     * @throws Exception
     */
    Long save(PurchaseOrderDO purchaseOrder) throws Exception;
}
