package com.eshop.purchase.dao.impl;

import com.eshop.common.util.DateProvider;
import com.eshop.purchase.dao.SupplierDAO;
import com.eshop.purchase.domain.SupplierDO;
import com.eshop.purchase.mapper.SupplierMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理DAO组件
 * @date 2022-04-12
 */
@Repository
public class SupplierDAOImpl implements SupplierDAO {

    /**
     * 供应商管理mapper组件
     */
    @Autowired
    private SupplierMapper supplierMapper;
    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;


    /**
     * 新增供应商
     * @param supplier 供应商
     */
    @Override
    public void save(SupplierDO supplier) throws Exception {
        supplier.setGmtCreate(dateProvider.getCurrentTime());
        supplier.setGmtModified(dateProvider.getCurrentTime());
        supplierMapper.save(supplier);
    }
}
