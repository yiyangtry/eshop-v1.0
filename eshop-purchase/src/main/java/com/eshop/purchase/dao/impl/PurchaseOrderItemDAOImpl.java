package com.eshop.purchase.dao.impl;

import com.eshop.common.util.DateProvider;
import com.eshop.purchase.dao.PurchaseOrderItemDAO;
import com.eshop.purchase.mapper.PurchaseOrderItemMapper;
import com.eshop.purchase.domain.PurchaseOrderItemDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单条目管理DAO组件
 * @date 2022-04-12
 */
@Repository
public class PurchaseOrderItemDAOImpl implements PurchaseOrderItemDAO {

    /**
     * 采购单条目管理mapper组件
     */
    @Autowired
    private PurchaseOrderItemMapper purchaseOrderItemMapper;

    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 批量新增采购单条目
     * @param purchaseOrderId 采购单id
     * @param purchaseOrderItems 采购单条目
     * @throws Exception
     */
    @Override
    public void batchSave(Long purchaseOrderId, List<PurchaseOrderItemDO> purchaseOrderItems) throws Exception {
        for(PurchaseOrderItemDO purchaseOrderItem : purchaseOrderItems) {
            purchaseOrderItem.setPurchaseOrderId(purchaseOrderId);
            purchaseOrderItem.setGmtCreate(dateProvider.getCurrentTime());
            purchaseOrderItem.setGmtModified(dateProvider.getCurrentTime());
            purchaseOrderItemMapper.save(purchaseOrderItem);
        }
    }
}
