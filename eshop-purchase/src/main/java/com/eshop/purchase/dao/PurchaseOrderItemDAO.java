package com.eshop.purchase.dao;

import com.eshop.purchase.domain.PurchaseOrderItemDO;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单条目管理DAO接口
 * @date 2022-04-12
 */
public interface PurchaseOrderItemDAO {

    /**
     * 批量新增采购单条目
     * @param purchaseOrderId 采购单id
     * @param purchaseOrderItems 采购单条目
     * @throws Exception
     */
    void batchSave(Long purchaseOrderId, List<PurchaseOrderItemDO> purchaseOrderItems) throws Exception;
}
