package com.eshop.purchase.dao.impl;

import com.eshop.common.util.DateProvider;
import com.eshop.purchase.dao.PurchaseOrderDAO;
import com.eshop.purchase.domain.PurchaseOrderDO;
import com.eshop.purchase.mapper.PurchaseOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理DAO组件
 * @date 2022-04-12
 */
@Repository
public class PurchaseOrderDAOImpl implements PurchaseOrderDAO {

    /**
     * 采购单管理mapper组件
     */
    @Autowired
    private PurchaseOrderMapper purchaseOrderMapper;
    /**
     * 日期辅助组件
     */
    @Autowired
    private DateProvider dateProvider;

    /**
     * 新增采购单
     * @param purchaseOrder 采购单
     */
    @Override
    public Long save(PurchaseOrderDO purchaseOrder) throws Exception {
        purchaseOrder.setGmtCreate(dateProvider.getCurrentTime());
        purchaseOrder.setGmtModified(dateProvider.getCurrentTime());
        purchaseOrderMapper.save(purchaseOrder);
        return purchaseOrder.getId();
    }
}
