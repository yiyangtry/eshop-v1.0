package com.eshop.purchase.controller;

import com.eshop.purchase.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理controller组件
 * @date 2022-04-11
 */
@RestController
@RequestMapping("/order")
public class PurchaseOrderController {

    /**
     * 采购单管理service组件
     */
    @Autowired
    private PurchaseOrderService purchaseOrderService;


}
