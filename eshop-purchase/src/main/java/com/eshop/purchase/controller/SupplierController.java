package com.eshop.purchase.controller;

import com.eshop.purchase.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理controller组件
 * @date 2022-04-12
 */
@Slf4j
@RestController
@RequestMapping("supplier")
public class SupplierController {

    /**
     * 供应商管理service组件
     */
    @Autowired
    private SupplierService supplierService;


}
