package com.eshop.purchase.domain.dto;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商
 * @date 2022-04-12
 */
@Data
public class SupplierDTO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 供应商名称
     */
    private String name;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 公司地址
     */
    private String companyAddress;
    /**
     * 联系人
     */
    private String contactor;
    /**
     * 联系人电话号码
     */
    private String contactorPhoneNumber;
    /**
     * 结算周期
     */
    private Integer settlementPeriod;
    /**
     * 开户银行名称
     */
    private String bankName;
    /**
     * 开户银行账号
     */
    private String bankAccount;
    /**
     * 开户银行账号持有人
     */
    private String bankAccountHolder;
    /**
     * 发票抬头
     */
    private String invoiceTitle;
    /**
     * 纳税人识别号
     */
    private String taxpayerId;
    /**
     * 经营范围
     */
    private String businessScope;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
