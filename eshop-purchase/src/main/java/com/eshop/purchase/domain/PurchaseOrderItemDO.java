package com.eshop.purchase.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购条目DTO类
 * @date 2022-04-11
 */
@Data
public class PurchaseOrderItemDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 采购单id
     */
    private Long purchaseOrderId;
    /**
     * 商品sku id
     */
    private Long goodsSkuId;
    /**
     * 采购数量
     */
    private Long purchaseCount;
    /**
     * 采购价格
     */
    private Double purchasePrice;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
