package com.eshop.purchase.domain;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单DO
 * @date 2022-04-10
 */
@Data
public class PurchaseOrderDO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 供应商id
     */
    private Long supplierId;
    /**
     * 预期到货时间
     */
    private Date expectArrivalTime;
    /**
     * 采购联系人
     */
    private String contactor;
    /**
     * 采购联系人电话号码
     */
    private String contactorPhoneNumber;
    /**
     * 采购联系人邮箱地址
     */
    private String contactorEmail;
    /**
     * 采购单备注
     */
    private String remark;
    /**
     * 采购员
     */
    private String purchaser;
    /**
     * 采购单的状态
     */
    private Integer status;
    /**
     * 采购单的创建时间
     */
    private Date gmtCreate;
    /**
     * 采购单的修改时间
     */
    private Date gmtModified;
}
