package com.eshop.purchase.mapper;

import com.eshop.purchase.domain.SupplierDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 供应商管理mapper组件
 * @date 2022-04-12
 */
@Mapper
public interface SupplierMapper {

    /**
     * 新增供应商
     * @param supplier 供应商
     */
    @Insert("INSERT INTO purchase_supplier("
            + "name,"
            + "company_name,"
            + "company_address,"
            + "contactor,"
            + "contactor_phone_number,"
            + "settlement_period,"
            + "bank_name,"
            + "bank_account,"
            + "bank_account_holder,"
            + "invoice_title,"
            + "taxpayer_id,"
            + "business_scope,"
            + "remark,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{name},"
            + "#{companyName},"
            + "#{companyAddress},"
            + "#{contactor},"
            + "#{contactorPhoneNumber},"
            + "#{settlementPeriod},"
            + "#{bankName},"
            + "#{bankAccount},"
            + "#{bankAccountHolder},"
            + "#{invoiceTitle},"
            + "#{taxpayerId},"
            + "#{businessScope},"
            + "#{remark},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(SupplierDO supplier);
}
