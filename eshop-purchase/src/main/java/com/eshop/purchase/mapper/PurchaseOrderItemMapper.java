package com.eshop.purchase.mapper;

import com.eshop.purchase.domain.PurchaseOrderItemDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单条目管理mapper组件
 * @date 2022-04-12
 */
@Mapper
public interface PurchaseOrderItemMapper {

    /**
     * 新增采购单条目
     * @param purchaseOrderItem 采购单条目
     * @throws Exception
     */
    @Insert("INSERT INTO purchase_order_item("
            + "purchase_order_id,"
            + "goods_sku_id,"
            + "purchase_count,"
            + "purchase_price,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{purchaseOrderId},"
            + "#{goodsSkuId},"
            + "#{purchaseCount},"
            + "#{purchasePrice},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(PurchaseOrderItemDO purchaseOrderItem);
}
