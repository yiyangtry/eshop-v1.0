package com.eshop.purchase.mapper;

import com.eshop.purchase.domain.PurchaseOrderDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理mapper组件
 * @date 2022-04-12
 */
@Mapper
public interface PurchaseOrderMapper {

    /**
     * 新增采购单
     * @param purchaseOrder 采购单
     */
    @Insert("INSERT INTO purchase_order("
            + "supplier_id,"
            + "expect_arrival_time,"
            + "contactor,"
            + "contactor_phone_number,"
            + "contactor_email,"
            + "remark,"
            + "purchaser,"
            + "status,"
            + "gmt_create,"
            + "gmt_modified"
            + ") VALUES("
            + "#{supplierId},"
            + "#{expectArrivalTime},"
            + "#{contactor},"
            + "#{contactorPhoneNumber},"
            + "#{contactorEmail},"
            + "#{remark},"
            + "#{purchaser},"
            + "#{status},"
            + "#{gmtCreate},"
            + "#{gmtModified}"
            + ")")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    void save(PurchaseOrderDO purchaseOrder);
}
