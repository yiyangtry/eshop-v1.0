package com.eshop.purchase;

import com.eshop.common.util.DateProvider;
import com.eshop.purchase.dao.PurchaseOrderDAO;
import com.eshop.purchase.domain.PurchaseOrderDO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.when;

/**
 * @author Elvis
 * @version 1.0
 * @description: 采购单管理DAO组件的单元测试类
 * @date 2022-04-12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseOrderDaoTest {

    /**
     * 日期辅助组件
     */
    @MockBean
    private DateProvider dateProvider;
    /**
     * 采购单管理DAO组件
     */
    @Autowired
    private PurchaseOrderDAO purchaseOrderDAO;

    /**
     * 初始化
     * @throws Exception
     */
    @Before
    public void setup() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        when(dateProvider.getCurrentTime()).thenReturn(formatter.parse(formatter.format(new Date())));
    }

    /**
     * 测试新建采购单
     * @throws Exception
     */
    @Test
    public void testSave() throws Exception {
        PurchaseOrderDO expectedPurchaseOrder = createPurchaseOrder();
    }

    /**
     * 创建采购单
     * @return 采购单
     * @throws Exception
     */
    private PurchaseOrderDO createPurchaseOrder() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        PurchaseOrderDO purchaseOrder = new PurchaseOrderDO();
        purchaseOrder.setSupplierId(1L);
        purchaseOrder.setExpectArrivalTime(formatter.parse("2022-04-12 10:00:00"));
        purchaseOrder.setContactor("张三");
        purchaseOrder.setContactorPhoneNumber("测试电话号码");
        purchaseOrder.setContactorEmail("测试邮箱地址");
        purchaseOrder.setRemark("测试采购单");
        purchaseOrder.setPurchaser("张三");
        purchaseOrder.setStatus(1);

        purchaseOrderDAO.save(purchaseOrder);

        return purchaseOrder;
    }
}
