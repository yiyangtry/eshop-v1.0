package com.eshop.promotion.domain.dto;

import com.eshop.common.util.AbstractObject;
import lombok.Data;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券
 * @date 2022-04-16
 */
@Data
public class CouponDTO extends AbstractObject {

    /**
     * id
     */
    private Long id;
    /**
     * 优惠券名称
     */
    private String name;
    /**
     * 优惠券类型
     */
    private Integer type;
    /**
     * 优惠券使用规则
     */
    private String rule;
    /**
     * 有效期开始时间
     */
    private Date validStartTime;
    /**
     * 有效期结束时间
     */
    private Date validEndTime;
    /**
     * 发行总数量
     */
    private Long giveOutCount;
    /**
     * 已经领取的数量
     */
    private Long receivedCount;
    /**
     * 发行方式
     */
    private Integer giveOutType;
    /**
     * 优惠券状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;

}