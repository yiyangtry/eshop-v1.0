package com.eshop.schedule.domain.dto;

import com.eshop.order.domain.dto.OrderItemDTO;
import com.eshop.schedule.domain.dto.ScheduleOrderPickingItemDTO;
import com.eshop.schedule.domain.dto.ScheduleOrderSendOutDetailDTO;
import lombok.Data;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 调度销售出库的结果
 * @date 2022-04-16
 */
@Data
public class SaleDeliveryScheduleResult {

	/**
	 * 订单条目
	 */
	private OrderItemDTO orderItem;
	/**
	 * 拣货条目
	 */
	private List<ScheduleOrderPickingItemDTO> pickingItems;
	/**
	 * 发货明细
	 */
	private List<ScheduleOrderSendOutDetailDTO> sendOutDetails;

}
